class Config {
  static const bool PLUS_VERSION = false;
  static const bool IS_DEBUG = false;
  static const bool SHOW_ADS = true;
  static const String SEARCH_FOLDRE = "kasait_search";

  // ANDROID
  static const String ADMOB_APP_ID = "ca-app-pub-3504979136002068~2775992641";
  static const String BANNAR = "ca-app-pub-3504979136002068/7725066997";
  static const String INTERSTITIAL = "ca-app-pub-3504979136002068/2608471689";
  static const String REWARD = "ca-app-pub-3504979136002068/2921486392";
  static const String APP_MARKET_ID = "https://play.google.com/store/apps/details?id=com.hamad.kasait";

  // IOS
  // static const String ADMOB_APP_ID = "ca-app-pub-3504979136002068~8197917706";
  // static const String BANNAR = "ca-app-pub-3504979136002068/9649109482";
  // static const String INTERSTITIAL = "ca-app-pub-3504979136002068/7378169367";
  // static const String REWARD = "ca-app-pub-3504979136002068/6650221682";
  // static const String APP_MARKET_ID = "itms-apps://itunes.apple.com/app/id1461425223";

}
