import 'dart:async';
import 'dart:io';

import 'package:cassette/BackendAPI/artistsAPI.dart';
import 'package:cassette/BackendAPI/tracksAPI.dart';
import 'package:cassette/config.dart';
import 'package:cassette/modules/modules.dart';
import 'package:cassette/modules/search_filter.dart';
import 'package:cassette/reusables/artist_item.dart';
import 'package:cassette/reusables/bottom_bar.dart';
import 'package:cassette/reusables/fetch_loading.dart';
import 'package:cassette/reusables/mini_player.dart';
import 'package:cassette/reusables/track_item.dart';
import 'package:flutter/material.dart';

import 'package:cassette/reusables/colorful_background.dart';
import 'package:path_provider/path_provider.dart';
import 'package:scoped_model/scoped_model.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: ScopedModel<FetchLoadingModel>(
        model: FetchLoadingModel()..initScrollController(),
        child: new SearchField(),
      ),
    );
  }
}

class SearchField extends StatefulWidget {
  const SearchField({Key key}) : super(key: key);

  @override
  _SearchFieldState createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  ArtistsAPI artistsAPI = new ArtistsAPI();
  TracksAPI tracksAPI = new TracksAPI();

  Artists artistsSearch;
  Tracks tracksSearch;

  bool isSearching = false;
  bool isThereArtists = false;
  bool isThereTracks = false;

  String searchFileContents = "";

  TextEditingController textCtrl = new TextEditingController();
  List<String> suggestions = [];
  StreamSubscription wait;

  @override
  void initState() {
    FetchLoadingModel.of(context).initCallback(_getMoreTracks);

    _loadSearchFile();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: const EdgeInsets.only(top: 55.0),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                buildTextField(),
                buildSuggestionsList(),
                buildResults(),
              ],
            ),
            Positioned(
              bottom: 300.0,
              right: (MediaQuery.of(context).size.width / 2) - 25,
              child: buildLoadingIndicator(),
            ),
            isKeyboardClosed() ? new MiniPlayer() : Container(),
            isKeyboardClosed()
                ? new BottomBar(page: NavPage.search)
                : Container(),
          ],
        ),
      ),
    );
  }

  ScopedModelDescendant<FetchLoadingModel> buildLoadingIndicator() {
    return ScopedModelDescendant<FetchLoadingModel>(
      builder: (context, child, model) {
        return model.loadingIndicator();
      },
    );
  }

  _getMoreTracks() async {
    // fetch more if next page
    if (tracksSearch.next == null) return;

    Tracks newTracksSearch = await tracksAPI.fetchMore(tracksSearch.next);

    // assign tracksSearch to each track in it
    // to help playlist to handle next and previous
    newTracksSearch.results.forEach((t) {
      return t.playlistMeta = new PlaylistMeta(
          -1, "getRandomTrack", tracksSearch, TrackPlaylistType.TRACKS);
    });

    setState(() {
      tracksSearch.results.addAll(newTracksSearch.results);
      tracksSearch.next = newTracksSearch.next;
      tracksSearch.previous = newTracksSearch.previous;
    });
  }

  Widget buildResults() {
    if (isThereArtists || isThereTracks) {
      return Flexible(
        child: ListView(
          controller: FetchLoadingModel.of(context).scrollController,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 18.0, right: 18.0),
              child: isThereArtists
                  ? Text(
                      "فنانين",
                      style: Theme.of(context).textTheme.headline,
                    )
                  : Container(),
            ),
            isThereArtists ? new ArtistResults(artistsSearch) : Container(),
            Divider(),
            Padding(
              padding:
                  const EdgeInsets.only(top: 18.0, bottom: 18.0, right: 18.0),
              child: isThereTracks
                  ? Text(
                      "اغاني",
                      style: Theme.of(context).textTheme.headline,
                    )
                  : Container(),
            ),
            isThereTracks ? new SongResults(tracksSearch) : Container(),
            SizedBox(
              height: 200,
            ),
          ],
        ),
      );
    } else if (!isSearching &&
        textCtrl.text.isNotEmpty &&
        suggestions.isEmpty) {
      return Padding(
        padding: const EdgeInsets.only(top: 13.0),
        child: Text("لا يوجد نتائج"),
      );
    } else {
      return Container();
    }
  }

  Widget buildSuggestionsList() {
    return suggestions.isNotEmpty
        ? Flexible(
            child: ListView.builder(
              itemCount: suggestions.length + 1,
              itemBuilder: (BuildContext context, int index) {
                if (index == suggestions.length) {
                  return SizedBox(
                    height: 200,
                  );
                }

                return ListTile(
                  leading: Icon(Icons.search),
                  title: Text(suggestions[index]),
                  onTap: () {
                    _tapSubmit(suggestions[index]);
                  },
                );
              },
            ),
          )
        : Container();
  }

  TextField buildTextField() {
    return TextField(
      controller: textCtrl,
      textInputAction: TextInputAction.search,
      onSubmitted: _onSubmit,
      onChanged: (value) async {
        setState(() {
          isSearching = true;
        });

        if (wait != null) wait.cancel();
        wait =
            Future.delayed(Duration(milliseconds: 500)).asStream().listen((v) {
          _buildSuggestions(value);
        });
      },
      decoration: InputDecoration(
        hintText: "ابحث",
        prefixIcon: IconButton(
          onPressed: _clearText,
          icon: Icon(Icons.clear),
        ),
        suffixIcon: Icon(Icons.search),
      ),
    );
  }

  bool isKeyboardClosed() {
    return MediaQuery.of(context).viewInsets.bottom == 0;
  }

  void _clearText() {
    // if (wait != null) {
    //   wait.cancel();
    // }

    if (textCtrl.text.isEmpty) {
      Navigator.pop(context);
    }
    setState(() {
      suggestions.clear();
      isThereArtists = false;
      isThereTracks = false;

      artistsSearch?.results?.clear();
      tracksSearch?.results?.clear();

      // textCtrl.clear(); throws error
      // workaround
      WidgetsBinding.instance.addPostFrameCallback((_) => textCtrl.clear());
    });
  }

  Future _loadSearchFile() async {
    if (searchFileContents.isEmpty) {
      final appDir = await getApplicationDocumentsDirectory();
      final myDir = new Directory('${appDir.path}/${Config.SEARCH_FOLDRE}');
      var file = File(myDir.path + "/search.txt");
      // Read the file.
      searchFileContents = await file.readAsString();
    }
  }

  Future<List<String>> _searchInFile(String value) {
    List<String> texts = value.split(" ");
    String text = texts.map((t) => "(?=.*" + t + ")").join();
    debugPrint("text: $text");

    // (?=.*foo)(?=.*baz)
    RegExp regExp = new RegExp(
      r"^.*" + text + r".*$",
      caseSensitive: false,
      multiLine: true,
    );

    Iterable<RegExpMatch> searchResults = regExp.allMatches(searchFileContents);

    return Future<List<String>>.value(searchResults
        .take(10)
        .map((s) => s.group(0).replaceFirst(RegExp(r"^.*~@"), ""))
        .toList());
  }

  _buildSuggestions(String value) async {
    suggestions.clear();

    setState(() {
      isSearching = true;
      isThereArtists = false;
      isThereTracks = false;
    });

    if (value.isEmpty || value.length == 1) {
      setState(() {});
      return;
    }

    // filter search input
    value = SearchFilter.filter(value);
    print("Filter: $value");

    var stopwatch = Stopwatch()..start();
    List<String> searchInFileResults = await _searchInFile(value);
    debugPrint("searchResults in ${stopwatch.elapsed}:");
    suggestions.addAll(searchInFileResults);
    debugPrint("_buildSuggestions done");
  }

  Future _tapSubmit(String value) async {
    setState(() {
      textCtrl.text = value;
      isSearching = true;
    });

    // to close keyboard
    FocusScope.of(context).requestFocus(new FocusNode());

    artistsSearch = await artistsAPI.search(value);
    tracksSearch = await tracksAPI.genericSearch(value);

    _showFinalResults(value);
  }

  Future _onSubmit(String value) async {
    setState(() {
      isSearching = true;
    });

    // filter search input
    value = SearchFilter.filter(value);
    print("Filter: $value");
    artistsSearch = await artistsAPI.search(value);
    tracksSearch = await tracksAPI.genericSearch(value);

    if (artistsSearch.results.isEmpty && tracksSearch.results.isEmpty) {
      // add to feedback
      tracksAPI.createFeedback("Search: $value");
    }

    // if empty results then separate letters
    value = SearchFilter.separateLetters(value);
    print("separateLetters: $value");

    if (artistsSearch.results.isEmpty) {
      artistsSearch = await artistsAPI.search(value);
    }

    if (tracksSearch.results.isEmpty) {
      tracksSearch = await tracksAPI.genericSearch(value);
    }

    _showFinalResults(value);
  }

  void _showFinalResults(String value) {
    if (artistsSearch.results.isEmpty && tracksSearch.results.isEmpty) {
      // do new search
      print(value);

      // add to feedback
      tracksAPI.createFeedback("Search: $value");
    }

    // assign tracksCtrl to each track in it
    // to help playlist to handle next and previous
    tracksSearch.results.forEach((t) {
      return t.playlistMeta = new PlaylistMeta(
          -1, "getRandomTrack", tracksSearch, TrackPlaylistType.TRACKS);
    });

    setState(() {
      isThereArtists = artistsSearch?.results?.isNotEmpty;
      isThereTracks = tracksSearch?.results?.isNotEmpty;
      isSearching = false;
      suggestions.clear();
    });
  }
}

class ArtistResults extends StatelessWidget {
  final Artists artistsSearch;

  const ArtistResults(this.artistsSearch, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.center,
      children: artistsSearch.results.map((a) {
        return Padding(
          /// padding 7 in ios was good
          /// but my oppo phone shows only on column
          padding: const EdgeInsets.all(0.0),
          child: ArtistItem(artist: a),
        );
      }).toList(),
    );
  }
}

class SongResults extends StatelessWidget {
  final Tracks tracksSearch;

  const SongResults(this.tracksSearch, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: tracksSearch.results.map((t) {
          return TrackItem(track: t);
        }).toList(),
      ),
    );
  }
}
