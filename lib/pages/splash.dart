import 'dart:io';

import 'package:cassette/BackendAPI/rest.dart';
import 'package:cassette/config.dart';
import 'package:cassette/modules/storage.dart';
import 'package:cassette/pages/landing.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:cassette/BackendAPI/authAPI.dart';
import 'package:cassette/pages/login.dart';
import 'package:cassette/reusables/colorful_background.dart';
import 'package:cassette/navigator.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';

class SplashPage extends StatelessWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _downloadSearchIndex(context);
    // _testSearch();

    // Future.delayed(Duration(seconds: 3)).then((v) {
    //   _newUserCheck(context);
    // });

    return Container(
      decoration: ColorfulBackground(),
      child: SplashScaffold(title: "كاسيت"),
    );
  }

  void _newUserCheck(BuildContext context) {
    // check if new user-> login as guest
    Storage.retrieveString("newUser").then((newUser) {
      if (newUser == null) {
        // new user
        // login as guest
        AuthAPI authAPI = new AuthAPI();
        authAPI.loginGuest().then((e) {
          if (e == null) {
            goTo(context, LandingPage());
          } else {
            print(e);
          }
        });
      } else {
        // if not a new user, check login if token is stored
        AuthAPI.checkLogin().then((isLoggedIn) {
          if (isLoggedIn) {
            goTo(context, LandingPage());
          } else {
            // if user is not new, must login to
            // avoid confusion when login as guest
            // without knowing that he logged out
            // from his account
            goTo(context, LoginPage());
          }
        });
      }
    });
  }

  _searchMD5Same() async {
    var response =
        await http.get(Rest.streamUrlSecure + "search/searchMD5.txt");
    String remoteMd5 = response.body;

    debugPrint("remoteMd5: " + remoteMd5);

    // first check if prev md5 is existed
    return Storage.contains("searchMD5").then((yes) {
      if (!yes) {
        // if it doesn't exist, create one
        // with the new remote md5 value
        Storage.storeString("searchMD5", remoteMd5).then((v) {
          debugPrint("Storage create new, return false");
          return false;
        });
      } else {
        // get local search md5
        return Storage.retrieveString("searchMD5").then((localMD5) async {
          debugPrint("remoteMd5: " + remoteMd5);
          debugPrint("localMD5: " + localMD5);

          // if same return true
          if (remoteMd5.trim() == localMD5.trim()) {
            debugPrint("Storage remoteMd5 == localMD5, return true");
            return true;
          } else {
            await Storage.storeString("searchMD5", remoteMd5);
            debugPrint("Storage remoteMd5 != localMD5, return false");
            return false;
          }
        });
      }

      return false;
    });
  }

  _downloadSearchIndex(BuildContext context) async {
    // first check the md5 sum
    // if different, then download the new file
    if (await _searchMD5Same()) {
      // _testSearch();
      _newUserCheck(context);
      return;
    }

    final appDir = await getApplicationDocumentsDirectory();
    final myDir = new Directory('${appDir.path}/${Config.SEARCH_FOLDRE}');
    final fileUrl = Rest.streamUrlSecure + "search/search.txt";

    myDir.exists().then((isThere) async {
      if (!isThere) {
        myDir.createSync(recursive: false);
        debugPrint(myDir.path);
      }

      final stopwatch = Stopwatch()..start();
      await FlutterDownloader.enqueue(
        url: fileUrl,
        savedDir: myDir.path,
        showNotification:
            false, // show download progress in status bar (for Android)
        openFileFromNotification:
            false, // click on notification to open downloaded file (for Android)
      );

      FlutterDownloader.registerCallback((id, status, progress) async {
        // print("$id $status $progress");

        debugPrint("progress:" + progress.toString());

        // if download is complete
        if (status.value == 3) {
          debugPrint("search.txt complete in ${stopwatch.elapsed}");
          // _testSearch();
          _newUserCheck(context);
        }
      });
    });
  }
}

class SplashScaffold extends StatelessWidget {
  SplashScaffold({Key key, this.title}) : super(key: key) {
    // showBanner();
  }

  final String title;
  final version = "v 2.1.3";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: const EdgeInsets.only(top: 140.0),
        child: Center(
          child: Column(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.46,
                  child: ClipRRect(
                    borderRadius: new BorderRadius.circular(13.0),
                    child: Image.asset("assets/images/icon_logo_ios.png"),
                  ),
                ),
                Text(
                  "كاسيت",
                  style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 30.0,
                  ),
                ),
                Text(
                  version,
                  style: TextStyle(
                    color: Colors.grey[700],
                    // fontSize: 14.0,
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
