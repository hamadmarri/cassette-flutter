import 'package:cassette/BackendAPI/authAPI.dart';
import 'package:cassette/navigator.dart';
import 'package:cassette/pages/landing.dart';
import 'package:cassette/pages/signup.dart';
import 'package:cassette/reusables/colorful_background.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String errorMsg = "";
  TextEditingController emailCtrl = new TextEditingController();
  TextEditingController passwordCtrl = new TextEditingController();

  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 21.0),
            child: Form(
              key: _formKey,
              autovalidate: _autoValidate,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 28.0),
                    child: Text(
                      "تسجيل دخول",
                      style: Theme.of(context).textTheme.headline,
                    ),
                  ),
                  _ErrorMsg(errorMsg: errorMsg),
                  _EmailField(emailCtrl),
                  _PasswordField(passwordCtrl),
                  Padding(
                    padding: const EdgeInsets.only(top: 80.0),
                    child: SizedBox(
                      width: 140.0,
                      child: RaisedButton(
                        color: Colors.blueAccent,
                        child: Text(
                          "دخول",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: _validateInputs,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 140.0,
                    child: RaisedButton(
                      child: Text("تسجيل جديد"),
                      onPressed: () {
                        goTo(context, SignupPage());
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 13.0),
                    child: GestureDetector(
                      child: Text("دخول زائر",
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Colors.blue)),
                      onTap: () {
                        AuthAPI authAPI = new AuthAPI();
                        authAPI.loginGuest().then((error) {
                          if (error != null) {
                            setState(() {
                              errorMsg = error;
                            });
                          } else {
                            goTo(context, LandingPage());
                          }
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _validateInputs() {
    final form = _formKey.currentState;
    if (form.validate()) {
      print(emailCtrl.text);
      print(passwordCtrl.text);
      // form.save();

      AuthAPI authAPI = new AuthAPI();
      authAPI.login(emailCtrl.text, passwordCtrl.text).then((error) {
        if (error != null) {
          setState(() {
            errorMsg = error;
          });
        } else {
          goTo(context, LandingPage());
        }
      });
    } else {
      setState(() => _autoValidate = true);
    }
  }
}

class _ErrorMsg extends StatelessWidget {
  const _ErrorMsg({Key key, @required this.errorMsg}) : super(key: key);

  final String errorMsg;

  @override
  Widget build(BuildContext context) {
    return Text(errorMsg, style: TextStyle(color: Colors.redAccent));
  }
}

class _EmailField extends StatefulWidget {
  _EmailField(this.emailCtrl);

  final TextEditingController emailCtrl;

  @override
  _EmailFieldState createState() => _EmailFieldState();
}

class _EmailFieldState extends State<_EmailField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.emailCtrl,
      validator: _validateEmail,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        //   border:
        //       OutlineInputBorder(borderSide: new BorderSide(color: Colors.teal)),
        labelText: 'الايميل',
        hintText: 'مثال music@gmail.com',
        // helperText: 'Keep it short, this is just a demo.',
      ),
    );
  }

  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "يرجى ادخال الايميل";
    }
    // This is just a regular expression for email addresses
    // String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
    //     "\\@" +
    //     "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
    //     "(" +
    //     "\\." +
    //     "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
    //     ")+";
    // RegExp regExp = new RegExp(p);

    // if (regExp.hasMatch(value)) {
    //   // So, the email is valid
    //   return null;
    // }

    // The pattern of the email didn't match the regex above.
    return null;
  }
}

class _PasswordField extends StatefulWidget {
  _PasswordField(this.passwordCtrl);

  final TextEditingController passwordCtrl;

  @override
  _PasswordState createState() => _PasswordState();
}

class _PasswordState extends State<_PasswordField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.passwordCtrl,
      obscureText: true,
      validator: _validatePassword,
      decoration: InputDecoration(
        //   border:
        //       OutlineInputBorder(borderSide: new BorderSide(color: Colors.teal)),
        labelText: 'كلمة السر',
        // helperText: 'Keep it short, this is just a demo.',
      ),
    );
  }

  String _validatePassword(String value) {
    if (value.isEmpty) {
      return "يرجى ادخال كلمة السر";
    }

    if (value.length < 6) {
      return "على الاقل 6 احرف";
    }

    return null;
  }
}
