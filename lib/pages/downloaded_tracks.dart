import 'dart:io';

import 'package:cassette/reusables/bottom_bar.dart';
import 'package:cassette/reusables/track_item.dart';
import 'package:flutter/material.dart';

import 'package:cassette/reusables/colorful_background.dart';
import 'package:cassette/reusables/mini_player.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

import '../modules/modules.dart';

class DownloadedTracks extends StatelessWidget {
  DownloadedTracks({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: DownloadedTracksScaffold(),
    );
  }
}

class DownloadedTracksScaffold extends StatefulWidget {
  const DownloadedTracksScaffold({Key key}) : super(key: key);

  @override
  _DownloadedTracksScaffold createState() => _DownloadedTracksScaffold();
}

class _DownloadedTracksScaffold extends State<DownloadedTracksScaffold> {
  List<Track> tracks = [];

  @override
  void initState() {
    _getDownloaded();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "الاغاني المحملة بالجهاز",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20.0,
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Flexible(
                child: ListView.builder(
                    itemCount: tracks.length + 1,
                    itemBuilder: (context, i) {
                      if (i == tracks.length) return SizedBox(height: 180.0);

                      return TrackItem(
                        track: tracks[i],
                        actions: _downloadedActions,
                      );
                    }),
              ),
            ],
          ),
          new MiniPlayer(),
          new BottomBar(page: NavPage.library),
        ],
      ),
    );
  }

  void _getDownloaded() async {
    final appDir = await getApplicationDocumentsDirectory();
    final myDir = new Directory('${appDir.path}/kasait');


    myDir.exists().then((isThere) {
      if (isThere) {
        print('exists');
      } else {
        print('non-existent');
        myDir.createSync(recursive: false);
        debugPrint(myDir.path);
      }

      Tracks tracksCtrl =
        new Tracks(myDir.listSync().length, null, null, List<Track>());
      List<Track> downloads = new List<Track>();
      List<Artist> artists = [Artist(-0, '', 0, 0, '', '')];

      for (var i = 0; i < myDir.listSync().length; i++) {
        File f = myDir.listSync()[i];
        String name = basename(f.path);
        name = name.substring(0, name.length - 4);

        // (-1 - i) is to make ids negatives (i.e. -1, -2, ...)
        Track track =
            new Track((-1 - i), name, artists, null, 0, f.path, null, null);

        track.playlistMeta = new PlaylistMeta(
            -1, "", tracksCtrl, TrackPlaylistType.DOWNLOADED_DEVICE);

        tracksCtrl.results.add(track);
        downloads.add(track);
      }

      setState(() {
        tracks.addAll(downloads);
      });
      
    });


    
  }

  List<Widget> _downloadedActions(Track track) {
    return <Widget>[
      new IconSlideAction(
        caption: 'حذف',
        color: Colors.redAccent,
        icon: Icons.delete,
        onTap: () {
          File f = new File(track.link);
          f.delete(recursive: false);
          setState(() {
            tracks.remove(track);
          });
        },
      ),
    ];
  }
}
