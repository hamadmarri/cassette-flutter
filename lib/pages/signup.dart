import 'package:cassette/BackendAPI/authAPI.dart';
import 'package:cassette/navigator.dart';
import 'package:cassette/pages/landing.dart';
import 'package:cassette/pages/login.dart';
import 'package:cassette/reusables/colorful_background.dart';
import 'package:flutter/material.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String errorMsg = "";
  TextEditingController emailCtrl = new TextEditingController();
  TextEditingController nameCtrl = new TextEditingController();
  TextEditingController passwordCtrl = new TextEditingController();
  TextEditingController password2Ctrl = new TextEditingController();

  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: Scaffold(
        appBar: AppBar(),
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 21.0),
            child: Form(
              key: _formKey,
              autovalidate: _autoValidate,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 28.0),
                    child: Text(
                      "تسجيل جديد",
                      style: Theme.of(context).textTheme.headline,
                    ),
                  ),
                  _ErrorMsg(errorMsg: errorMsg),
                  _EmailField(emailCtrl),
                  _NameField(nameCtrl),
                  _PasswordField(passwordCtrl),
                  _PasswordField(password2Ctrl),
                  Padding(
                    padding: const EdgeInsets.only(top: 13.0),
                    child: SizedBox(
                      width: 140.0,
                      child: RaisedButton(
                        color: Colors.blueAccent,
                        child: Text(
                          "تسجيل جديد",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: _validateInputs,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 13.0),
                    child: GestureDetector(
                      child: Text("تسجيل دخول",
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Colors.blue)),
                      onTap: () {
                        goTo(context, LoginPage());
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _validateInputs() {
    final form = _formKey.currentState;

    setState(() => errorMsg = '');

    if (passwordCtrl.text != password2Ctrl.text) {
      setState(() {
        errorMsg = "كلمة السر غير متطابقة";
        return _autoValidate = true;
      });
    }

    if (form.validate()) {
      AuthAPI authAPI = new AuthAPI();

      print(emailCtrl.text);
      print(passwordCtrl.text);
      // form.save();

      authAPI
          .signup(
        username: emailCtrl.text,
        password: passwordCtrl.text,
        firstname: nameCtrl.text,
      )
          .then((error) {
        if (error != null) {
          setState(() {
            errorMsg = error;
          });
        } else {
          // if register success, then login directly
          authAPI.login(emailCtrl.text, passwordCtrl.text).then((error) {
            if (error != null) {
              setState(() {
                errorMsg = error;
              });
            } else {
              // if login success, then go to landing
              goTo(context, LandingPage());
            }
          });
        }
      });
    } else {
      setState(() => _autoValidate = true);
    }
  }
}

class _ErrorMsg extends StatelessWidget {
  const _ErrorMsg({Key key, @required this.errorMsg}) : super(key: key);

  final String errorMsg;

  @override
  Widget build(BuildContext context) {
    return Text(errorMsg, style: TextStyle(color: Colors.redAccent));
  }
}

class _EmailField extends StatefulWidget {
  _EmailField(this.emailCtrl);

  final TextEditingController emailCtrl;

  @override
  _EmailFieldState createState() => _EmailFieldState();
}

class _EmailFieldState extends State<_EmailField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.emailCtrl,
      validator: _validateEmail,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        //   border:
        //       OutlineInputBorder(borderSide: new BorderSide(color: Colors.teal)),
        labelText: 'الايميل',
        hintText: 'مثال music@gmail.com',
        // helperText: 'Keep it short, this is just a demo.',
      ),
    );
  }

  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "يرجى ادخال الايميل";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'الايميل غير صحيح';
  }
}

class _NameField extends StatefulWidget {
  _NameField(this.nameCtrl);

  final TextEditingController nameCtrl;

  @override
  _NameFieldState createState() => _NameFieldState();
}

class _NameFieldState extends State<_NameField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.nameCtrl,
      validator: _validateName,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        //   border:
        //       OutlineInputBorder(borderSide: new BorderSide(color: Colors.teal)),
        labelText: 'الاسم',
        // hintText: 'احمد',
        // helperText: 'Keep it short, this is just a demo.',
      ),
    );
  }

  String _validateName(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "يرجى ادخال اسم";
    }

    if (value.length < 2 || value.length > 50) {
      return "من 2 الى 50 حرف";
    }

    return null;
  }
}

class _PasswordField extends StatefulWidget {
  _PasswordField(this.passwordCtrl);

  final TextEditingController passwordCtrl;

  @override
  _PasswordState createState() => _PasswordState();
}

class _PasswordState extends State<_PasswordField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.passwordCtrl,
      obscureText: true,
      validator: _validatePassword,
      decoration: InputDecoration(
        //   border:
        //       OutlineInputBorder(borderSide: new BorderSide(color: Colors.teal)),
        labelText: 'كلمة السر',
        // helperText: 'Keep it short, this is just a demo.',
      ),
    );
  }

  String _validatePassword(String value) {
    if (value.isEmpty) {
      return "يرجى ادخال كلمة السر";
    }

    if (value.length < 6) {
      return "على الاقل 6 احرف";
    }

    return null;
  }
}
