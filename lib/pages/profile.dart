// import 'package:admob_flutter/admob_flutter.dart';
import 'package:cassette/BackendAPI/userAPI.dart';
import 'package:cassette/reusables/bottom_bar.dart';
import 'package:cassette/reusables/track_item.dart';
import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import 'package:cassette/BackendAPI/tracksAPI.dart';
import 'package:cassette/reusables/colorful_background.dart';
import 'package:cassette/reusables/fetch_loading.dart';
import 'package:cassette/reusables/mini_player.dart';

import '../modules/modules.dart';

class ProfilePage extends StatelessWidget {
  ProfilePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: ScopedModel<FetchLoadingModel>(
        model: FetchLoadingModel()..initScrollController(),
        child: ProfilePageScaffold(),
      ),
    );
  }
}

class ProfilePageScaffold extends StatefulWidget {
  const ProfilePageScaffold({Key key}) : super(key: key);

  @override
  _ProfilePageScaffold createState() => _ProfilePageScaffold();
}

class _ProfilePageScaffold extends State<ProfilePageScaffold> {
  UserAPI userAPI;
  TracksAPI tracksAPI = new TracksAPI();
  Tracks tracksCtrl;
  List<Track> tracks = [];
  // final compact = NumberFormat.compact();

  @override
  void initState() {
    FetchLoadingModel.of(context).initCallback(_getMoreTracks);

    userAPI = new UserAPI()..fetchUserDetails().then((v) => _getTracks());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 13.0),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text("المفضلة", style: TextStyle(fontSize: 20.0)),
                ),
              ),
              Flexible(
                child: ListView.builder(
                    controller: FetchLoadingModel.of(context).scrollController,
                    itemCount: tracks.length + 1,
                    itemBuilder: (context, i) {
                      if (i == tracks.length) return SizedBox(height: 180.0);
                      return TrackItem(track: tracks[i]);
                    }),
              ),
            ],
          ),
          Positioned(
            bottom: 300.0,
            right: (MediaQuery.of(context).size.width / 2) - 25,
            child: buildLoadingIndicator(),
          ),
          new MiniPlayer(),
          new BottomBar(page: NavPage.profile),
        ],
      ),
    );
  }

  ScopedModelDescendant<FetchLoadingModel> buildLoadingIndicator() {
    return ScopedModelDescendant<FetchLoadingModel>(
      builder: (context, child, model) {
        return model.loadingIndicator();
      },
    );
  }

  void _getTracks() async {
    FetchLoadingModel.of(context).startLoading();
    tracksCtrl = await userAPI.fetchFaveSongs();

    // assign tracksCtrl to each track in it
    // to help playlist to handle next and previous
    tracksCtrl.results.forEach((t) {
      return t.playlistMeta =
          new PlaylistMeta(-1, "", tracksCtrl, TrackPlaylistType.TRACKS);
    });

    setState(() {
      tracks.addAll(tracksCtrl.results);
    });

    _getMoreTracks();

    FetchLoadingModel.of(context).finishLoading();
  }

  _getMoreTracks() async {
    // fetch more if next page
    if (tracksCtrl.next == null) return;

    tracksCtrl = await tracksAPI.fetchMore(tracksCtrl.next);

    // assign tracksCtrl to each track in it
    // to help playlist to handle next and previous
    tracksCtrl.results.forEach((t) {
      return t.playlistMeta = new PlaylistMeta(
          -1, "getRandomTrack", tracksCtrl, TrackPlaylistType.TRACKS);
    });

    setState(() {
      tracks.addAll(tracksCtrl.results);
    });
  }
}
