// import 'package:admob_flutter/admob_flutter.dart';
import 'package:cassette/reusables/bottom_bar.dart';
import 'package:cassette/reusables/track_item.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import 'package:cassette/BackendAPI/tracksAPI.dart';
import 'package:cassette/reusables/colorful_background.dart';
import 'package:cassette/reusables/fetch_loading.dart';
import 'package:cassette/reusables/mini_player.dart';

import '../modules/modules.dart';

class TracksList extends StatelessWidget {
  TracksList({Key key, this.tracksCtrl}) : super(key: key);

  final Tracks tracksCtrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: ScopedModel<FetchLoadingModel>(
        model: FetchLoadingModel()..initScrollController(),
        child: TracksListScaffold(tracksCtrl: tracksCtrl),
      ),
    );
  }
}

class TracksListScaffold extends StatefulWidget {
  const TracksListScaffold({Key key, this.tracksCtrl}) : super(key: key);

  final Tracks tracksCtrl;

  @override
  _TracksListScaffold createState() => _TracksListScaffold();
}

class _TracksListScaffold extends State<TracksListScaffold> {
  TracksAPI tracksAPI = new TracksAPI();
  Tracks tracksCtrl;
  List<Track> tracks = [];
  final compact = NumberFormat.compact();

  @override
  void initState() {
    tracksCtrl = widget.tracksCtrl;

    FetchLoadingModel.of(context).initCallback(_getMoreTracks);

    _getTracks();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Flexible(
                child: ListView.builder(
                    controller: FetchLoadingModel.of(context).scrollController,
                    itemCount: tracks.length + 1,
                    itemBuilder: (context, i) {
                      if (i == tracks.length) return SizedBox(height: 180.0);
                      return TrackItem(track: tracks[i]);
                    }),
              ),
            ],
          ),
          Positioned(
            bottom: 300.0,
            right: (MediaQuery.of(context).size.width / 2) - 25,
            child: buildLoadingIndicator(),
          ),
          new MiniPlayer(),
          new BottomBar(page: NavPage.others),
        ],
      ),
      // floatingActionButton: buildGoToTopButton(),
      // floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }

  ScopedModelDescendant<FetchLoadingModel> buildLoadingIndicator() {
    return ScopedModelDescendant<FetchLoadingModel>(
      builder: (context, child, model) {
        return model.loadingIndicator();
      },
    );
  }

  buildGoToTopButton() {
    return ScopedModelDescendant<FetchLoadingModel>(
      builder: (context, child, model) {
        debugPrint("buildGoToTopButton()");
        return model.showGoToTop ? model.showGoToTopButton() : Container();
      },
    );
  }

  void _getTracks() async {
    FetchLoadingModel.of(context).startLoading();

    setState(() {
      tracks.addAll(tracksCtrl.results);
    });

    _getMoreTracks();

    FetchLoadingModel.of(context).finishLoading();
  }

  _getMoreTracks() async {
    TrackPlaylistType playlistType =
        tracksCtrl.results[0].playlistMeta.playlistType;

    // fetch more if next page
    if (playlistType == TrackPlaylistType.TRACKS) {
      if (tracksCtrl.next != null) {
        tracksCtrl = await tracksAPI.fetchMore(tracksCtrl.next);

        // assign tracksCtrl to each track in it
        // to help playlist to handle next and previous
        tracksCtrl.results.forEach((t) {
          return t.playlistMeta = new PlaylistMeta(
              -1, "getRandomTrack", tracksCtrl, TrackPlaylistType.TRACKS);
        });
      }
      // if no more, then auto play must stop
      else {
        return null;
      }
    } else if (playlistType == TrackPlaylistType.RANDOM) {
      tracksCtrl = await tracksAPI.fetchRandom10();

      // assign tracksCtrl to each track in it
      // to help playlist to handle next and previous
      tracksCtrl.results.forEach((t) {
        return t.playlistMeta = new PlaylistMeta(
            -1, "getRandomTrack", tracksCtrl, TrackPlaylistType.RANDOM);
      });
    } else if (playlistType == TrackPlaylistType.DOWNLOADED_LIST) {
      if (tracksCtrl.next != null) {
        tracksCtrl = await tracksAPI.fetchMoreDownloaded(tracksCtrl.next);

        // assign tracksCtrl to each track in it
        // to help playlist to handle next and previous
        tracksCtrl.results.forEach((t) {
          return t.playlistMeta = new PlaylistMeta(-1, "getRandomTrack",
              tracksCtrl, TrackPlaylistType.DOWNLOADED_LIST);
        });
      }
      // if no more, then auto play must stop
      else {
        return null;
      }
    }

    setState(() {
      tracks.addAll(tracksCtrl.results);
    });
  }
}
