import 'package:cassette/BackendAPI/authAPI.dart';
// import 'package:cassette/modules/ads.dart';
import 'package:cassette/modules/storage.dart';
// import 'package:cassette/pages/auth.dart';
import 'package:cassette/pages/home.dart';
import 'package:cassette/pages/landing.dart';
import 'package:cassette/pages/login.dart';
import 'package:cassette/pages/search.dart';
import 'package:cassette/pages/signup.dart';
// import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';

import 'package:share/share.dart';

import 'package:cassette/reusables/colorful_background.dart';

// import 'package:flare_flutter/flare_actor.dart';

import 'package:cassette/navigator.dart';

// import '../config.dart';
// import 'package:cassette/pages/auth.dart';

class IndexPage extends StatelessWidget {
  IndexPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthAPI.checkLogin().then((isLoggedIn) {
      if (isLoggedIn) {
        // goTo(context, LandingPage());
      } else {
        goTo(context, LoginPage());
      }
    });

    return Container(
      decoration: ColorfulBackground(),
      child: IndexScaffold(title: "كاسيت"),
    );
  }
}

class IndexScaffold extends StatelessWidget {
  IndexScaffold({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: const EdgeInsets.only(top: 60.0),
        child: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                RaisedButton(
                  child: Text("Landing"),
                  onPressed: () {
                    goTo(context, LandingPage());
                  },
                ),
                RaisedButton(
                  child: Text("Home"),
                  onPressed: () {
                    goTo(context, Home());
                  },
                ),
                RaisedButton(
                  child: Text("clear storage"),
                  onPressed: () {
                    Storage.clear();
                    // goTo(context, LocalStoragePage());
                  },
                ),
                RaisedButton(
                  child: Text("share"),
                  onPressed: () {
                    Share.share(":D");
                  },
                ),
                RaisedButton(
                  child: Text("Sign in"),
                  onPressed: () {
                    // goTo(context, AuthPage());
                  },
                ),
                RaisedButton(
                  child: Text("Search"),
                  onPressed: () {
                    goTo(context, SearchPage());
                  },
                ),
                RaisedButton(
                  child: Text("login"),
                  onPressed: () {
                    goTo(context, LoginPage());
                  },
                ),
                RaisedButton(
                  child: Text("signin"),
                  onPressed: () {
                    goTo(context, SignupPage());
                  },
                ),
                // new Lo(),
              ]),
        ),
      ),
    );
  }
}

// class Lo extends StatefulWidget {
//   @override
//   _LoState createState() => _LoState();
// }

// // class _LoState extends State<Lo> {
// //   @override
// //   Widget build(BuildContext context) {
// //     return SizedBox(
// //       height: 100,
// //       child: new FlareActor(
// //         "assets/l.flr",
// //         alignment: Alignment.center,
// //         fit: BoxFit.fitHeight,
// //         animation: "go",
// //       ),
// //     );
// //   }
// // }
