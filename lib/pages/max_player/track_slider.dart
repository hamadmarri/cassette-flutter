part of './max_player.dart';

class _TrackSlider extends StatefulWidget {
  const _TrackSlider({Key key}) : super(key: key);

  @override
  __TrackSliderState createState() => __TrackSliderState();
}

class __TrackSliderState extends State<_TrackSlider> {
  double _value = 0;

  // when user tap and seek
  bool onChange = false;
  Timer timer;

  @override
  void initState() {
    timer = Timer.periodic(Duration(milliseconds: 80), _updatePositionSeekBar);
    super.initState();
  }

  @override
  dispose() {
    timer.cancel();
    super.dispose();
  }

  void _updatePositionSeekBar(Timer timer) {
    if (!mounted) return;

    if (PlayerModel.of(context).position == null) return;

    if (!onChange) {
      setState(() {
        if (PlayerModel.of(context).position == null ||
        PlayerModel.of(context).duration == null) {
          _value = 0.0;
        } else {
        _value = PlayerModel.of(context).position.inMilliseconds /
            PlayerModel.of(context).duration.inMilliseconds;
        _value = _value > 1.0 ? 1.0 : _value;
        }

      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 34.0, left: 21.0, right: 21.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Directionality(
            textDirection: TextDirection.ltr,
            child: Slider(
              activeColor: Colors.pink[400],
              inactiveColor: Colors.pink[100],
              onChanged: (double value) {
                if (PlayerModel.of(context).currentTrack == null) return;

                setState(() {
                  _value = value;
                });
              },
              onChangeStart: (value) {
                onChange = true;
              },
              onChangeEnd: (value) async {
                if (PlayerModel.of(context).currentTrack == null) return;

                int toMilli =
                    (PlayerModel.of(context).duration.inMilliseconds * value)
                        .round();
                PlayerModel.of(context).audioPlayer.seek(
                      Duration(milliseconds: toMilli),
                    );
                await Future.delayed(Duration(seconds: 2));
                onChange = false;
              },
              value: _value,
            ),
          ),
          Flexible(
            fit: FlexFit.loose,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new _DurationText(),
                  new _PositionText(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _PositionText extends StatelessWidget {
  const _PositionText({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<PlayerModel>(
      builder: (BuildContext context, Widget child, PlayerModel model) {
        if (model.position != null) {
          return Text(_printDuration(model.position));
        } else {
          return Text("--:--");
        }
      },
    );
  }
}

class _DurationText extends StatelessWidget {
  const _DurationText({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<PlayerModel>(
      builder: (BuildContext context, Widget child, PlayerModel model) {
        // print("_durationText");
        if (model.duration != null) {
          return Text(_printDuration(model.duration));
        } else {
          return Text("--:--");
        }
      },
    );
  }
}

String _printDuration(Duration duration) {
  String twoDigits(int n) {
    if (n >= 10) return "$n";
    return "0$n";
  }

  String twoDigitHours = twoDigits(duration.inHours);
  String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));

  if (twoDigitHours == "00")
    return "$twoDigitMinutes:$twoDigitSeconds";
  else
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
}
