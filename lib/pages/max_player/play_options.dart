part of './max_player.dart';

class _PlayOptions extends StatelessWidget {
  const _PlayOptions({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 34.0),
      child: SizedBox(
        width: 200,
        child: RaisedButton(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          color: Colors.pink[400],
          textColor: Colors.white,
          elevation: 0.0,
          onPressed: () {
            PlayListModel.of(context).changeMode();
          },
          child: ScopedModelDescendant<PlayListModel>(
            builder: (BuildContext context, Widget child, PlayListModel model) {
              if (model.mode == PlayOptions.REPEATE_LIST) {
                return _PlayOtionsText(
                  text: "تشغيل تلقائي",
                  iconData: Icons.repeat,
                );
              } else if (model.mode == PlayOptions.REPEATE_ONCE) {
                return _PlayOtionsText(
                  text: "تشغيل تلقائي",
                  iconData: Icons.repeat_one,
                );
              } else if (model.mode == PlayOptions.SHUFFLE ||
                  model.mode == PlayOptions.RADIO) {
                return _PlayOtionsText(
                  text: "تشغيل عشوائي",
                  iconData: Icons.shuffle,
                );
              } else {
                return _PlayOtionsText(
                  text: "ايقاف",
                  iconData: Icons.do_not_disturb_off,
                );
              }
            },
          ),
        ),
      ),
    );
  }
}

class _PlayOtionsText extends StatelessWidget {
  const _PlayOtionsText({Key key, this.text, this.iconData}) : super(key: key);

  @required
  final String text;
  @required
  final IconData iconData;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(text, style: TextStyle(fontSize: 21)),
        Icon(iconData, size: 26),
      ],
    );
  }
}
