part of './max_player.dart';

class _ArtistName extends StatelessWidget {
  const _ArtistName({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<PlayerModel>(
        builder: (BuildContext context, Widget child, PlayerModel model) {
      if (model.currentTrack != null) {
        return GestureDetector(
          onTap: () {
            goTo(context, ArtistPage(artist: model.currentTrack.artists[0]));
          },
          child: Text(
            model.currentTrack.artists[0].name,
            style: TextStyle(
              color: Color(0xff0388A6),
              fontSize: 20.0,
            ),
          ),
        );
      } else {
        return Container();
      }
    });
  }
}
