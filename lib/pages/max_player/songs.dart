part of './max_player.dart';

class _Songs extends StatefulWidget {
  const _Songs({Key key}) : super(key: key);

  @override
  _SongsState createState() => _SongsState();
}

class _SongsState extends State<_Songs> {
  TracksAPI tracksAPI = new TracksAPI();
  List<Track> tracks = [];

  @override
  initState() {
    tracks.addAll(
        PlayerModel.of(context).currentTrack.playlistMeta.tracksCtrl.results);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: tracks.map((t) {
          return TrackItem(track: t);
        }).toList(),
      ),
    );
  }
}
