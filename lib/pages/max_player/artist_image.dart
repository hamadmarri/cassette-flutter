part of './max_player.dart';

class _ArtistImage extends StatefulWidget {
  const _ArtistImage({Key key}) : super(key: key);

  @override
  __ArtistImageState createState() => __ArtistImageState();
}

class __ArtistImageState extends State<_ArtistImage> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: _minAxis(context) * 0.55,
      width: _minAxis(context) * 0.55,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ClipRRect(
            borderRadius: new BorderRadius.circular(13.0),
            child: ScopedModelDescendant<PlayerModel>(builder:
                (BuildContext context, Widget child, PlayerModel model) {
              if (model.currentTrack != null &&
                  model.currentTrack.artists[0].file.isNotEmpty) {
                return Image.network(
                  model.currentTrack.artists[0].file,
                  fit: BoxFit.cover,
                );
              } else {
                return Image.asset("assets/images/icon_logo_ios.png");
              }
            }),
          ),
        ],
      ),
    );
  }

  double _minAxis(BuildContext context) {
    return min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
  }
}
