import 'dart:async';
import 'dart:io';
import 'dart:math';

// import 'package:admob_flutter/admob_flutter.dart';
import 'package:cassette/BackendAPI/authAPI.dart';
import 'package:cassette/BackendAPI/tracksAPI.dart';
import 'package:cassette/BackendAPI/userAPI.dart';
import 'package:cassette/modules/ads.dart';
import 'package:cassette/modules/limit.dart';
import 'package:cassette/modules/maxplayer_model.dart';
import 'package:cassette/modules/modules.dart';
import 'package:cassette/modules/play_list.dart';
import 'package:cassette/modules/player.dart';
import 'package:cassette/modules/share.dart';
import 'package:cassette/navigator.dart';
import 'package:cassette/pages/artist.dart';
import 'package:cassette/reusables/track_item.dart';
import 'package:cassette/reusables/transparent_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:cassette/reusables/colorful_background.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../config.dart';
import '../timeout.dart';

part './artist_image.dart';
part './track_slider.dart';
part './track_name.dart';
part './artist_name.dart';
part './controls.dart';
part './actions.dart';
part './play_options.dart';
part './songs.dart';

class MaxPlayer extends StatefulWidget {
  MaxPlayer() {
    TransparentAppBar.appBar('');
  }

  @override
  _MaxPlayerState createState() => _MaxPlayerState();
}

class _MaxPlayerState extends State<MaxPlayer> {
  @override
  void initState() {
    if (Config.SHOW_ADS) {
      Timer.periodic(new Duration(seconds: 1), (timer) {
        if (Limit.timeout && !Limit.navigated) {
          if (PlayerModel.of(context).isPlaying) {
            PlayerModel.of(context).pause();
          }
          goTo(context, TimeoutPage());
        }
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<MaxPlayerModel>(
      model: MaxPlayerModel(),
      child: Container(
        decoration: ColorfulBackground(),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(),
          body: new _Content(),
        ),
      ),
    );
  }
}

class _Content extends StatelessWidget {
  const _Content({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isThereTrack = PlayerModel.of(context).currentTrack != null;

    return SingleChildScrollView(
      child: Center(
        child: Padding(
          padding:
              EdgeInsets.only(top: 55), //(top: TransparentAppBar.height + 72),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new _ArtistImage(),
              new _TrackSlider(),
              new _TrackName(),
              new _ArtistName(),
              new _Controls(),
              isThereTrack ? new _Actions() : Container(),
              isThereTrack ? new Divider() : Container(),
              isThereTrack ? new _PlayOptions() : Container(),
              isThereTrack ? new _Songs() : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
