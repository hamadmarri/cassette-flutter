part of './max_player.dart';

class _Actions extends StatelessWidget {
  const _Actions({Key key}) : super(key: key);

  final double size = 28.0;

  @override
  Widget build(BuildContext context) {
    final Color color = Colors.blueGrey;

    return Padding(
      padding: const EdgeInsets.only(top: 21.0, left: 21.0, right: 21.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.more_horiz),
            color: color,
            iconSize: size,
            onPressed: () {
              _showActionSheet(context, PlayerModel.of(context).currentTrack);
            },
          ),
          new _Fave(color: color, size: size),
          new _Download(color: color, size: size),
        ],
      ),
    );
  }

  Future _showActionSheet(BuildContext context, Track track) {
    return showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) {
        return CupertinoActionSheet(
          title: ScopedModelDescendant<PlayerModel>(
              builder: (BuildContext context, Widget child, PlayerModel model) {
            return Text(
              model.currentTrack.name,
              style: TextStyle(
                fontSize: 20.0,
                color: Color(0xff02212E),
              ),
            );
          }),
          message: ScopedModelDescendant<PlayerModel>(
              builder: (BuildContext context, Widget child, PlayerModel model) {
            return Text(
              model.currentTrack.artists[0].name,
              style: TextStyle(
                fontSize: 18.0,
                color: Color(0xff02212E),
              ),
            );
          }),
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 55.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "شارك",
                      style: TextStyle(
                        color: Colors.lightBlue,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Icon(
                      Icons.share,
                      color: Colors.lightBlue,
                      size: 28.0,
                    ),
                  ],
                ),
              ),
              onPressed: () {
                ShareModule.shareTrack(track);
                Navigator.pop(context, 'Cancel');
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            child: const Text(
              'الغاء',
              style: TextStyle(
                color: Colors.lightBlue,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context, 'Cancel');
            },
          ),
        );
      },
    );
  }
}

class _Fave extends StatefulWidget {
  const _Fave({
    Key key,
    @required this.color,
    @required this.size,
  }) : super(key: key);

  final Color color;
  final double size;

  @override
  _FaveState createState() => _FaveState();
}

class _FaveState extends State<_Fave> {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MaxPlayerModel>(
      builder: (BuildContext context, Widget child, MaxPlayerModel model) {
        return IconButton(
          icon:
              model.faved ? Icon(Icons.favorite) : Icon(Icons.favorite_border),
          color: model.faved ? Colors.redAccent : widget.color,
          iconSize: widget.size,
          onPressed: () {
            // no fave for guests
            if (AuthAPI.isGuest) {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text("غير متاح للضيف"),
                ),
              );
              return;
            }

            UserAPI userAPI = new UserAPI();
            int trackid = PlayerModel.of(context).currentTrack.id;

            if (!model.faved) {
              userAPI.addToFavorite(trackid);
            } else {
              userAPI.removeFromFavorite(trackid);
            }

            setState(() {
              model.faved = !model.faved;
            });
          },
        );
      },
    );
  }
}

class _Download extends StatefulWidget {
  const _Download({
    Key key,
    @required this.color,
    @required this.size,
  }) : super(key: key);

  final Color color;
  final double size;

  @override
  _DownloadState createState() => _DownloadState();
}

class _DownloadState extends State<_Download> {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MaxPlayerModel>(
      builder: (BuildContext context, Widget child, MaxPlayerModel model) {
        _checkIfAlreadyDownloaded();
        return Stack(children: <Widget>[
          IconButton(
            icon: model.downloadComplete
                ? Icon(Icons.cloud_done)
                : Icon(Icons.cloud_download),
            color: model.downloadComplete ? Colors.blueAccent : widget.color,
            iconSize: widget.size,
            onPressed: () async {
              if (!model.downloadComplete) {
                if (Config.SHOW_ADS) {
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text("يجب متابعة الاعلان كاملا للتحميل"),
                    ),
                  );
                  await Future.delayed(Duration(seconds: 3));
                }

                AdsModel.of(context).showReward();
              }
            },
          ),
          ScopedModelDescendant<AdsModel>(
            builder: (BuildContext context, Widget child, AdsModel model) {
              if (model.rewarded) {
                model.rewarded = false;
                _download();
              }
              return Container();
            },
          ),
          (model.downloadProgress > -1)
              ? Positioned(
                  top: -3,
                  left: 0,
                  child: Center(
                    widthFactor: 2.0,
                    child: Text("${model.downloadProgress}%"),
                  ),
                )
              : Container(),
        ]);
      },
    );
  }

  Future _checkIfAlreadyDownloaded() async {
    if (MaxPlayerModel.of(context).downloadProgress != -1) return;

    final appDir = await getApplicationDocumentsDirectory();
    final myDir = new Directory('${appDir.path}/kasait');
    Track track = PlayerModel.of(context).currentTrack;

    myDir.exists().then((isThere) {
      if (isThere) {
        int i = myDir
            .listSync(recursive: false)
            .map((f) => path.basename(f.path))
            .toList()
            .indexOf(track.name + ".mp3");

        // print(i);
        if (i > -1) {
          setState(() {
            MaxPlayerModel.of(context).downloadComplete = true;
          });
        }
      }
    });
  }

  _download() async {
    final appDir = await getApplicationDocumentsDirectory();
    final myDir = new Directory('${appDir.path}/kasait');
    Track track = PlayerModel.of(context).currentTrack;
    final trackUrl = PlayerModel.of(context).constructStreamUrl(track);

    myDir.exists().then((isThere) async {
      if (!isThere) {
        myDir.createSync(recursive: false);
        debugPrint(myDir.path);
      }

      await FlutterDownloader.enqueue(
        url: trackUrl,
        savedDir: myDir.path,
        showNotification:
            true, // show download progress in status bar (for Android)
        openFileFromNotification:
            true, // click on notification to open downloaded file (for Android)
      );

      FlutterDownloader.registerCallback((id, status, progress) {
        // print("$id $status $progress");

        setState(() {
          MaxPlayerModel.of(context).downloadProgress = progress;
        });

        // if download is complete
        if (status.value == 3) {
          setState(() {
            MaxPlayerModel.of(context).downloadComplete = true;
            MaxPlayerModel.of(context).downloadProgress = -1;
          });
          TracksAPI tracksAPI = new TracksAPI();
          Track track = PlayerModel.of(context).currentTrack;
          tracksAPI.createDownload(track.id);
        }
      });
    });
  }
}
