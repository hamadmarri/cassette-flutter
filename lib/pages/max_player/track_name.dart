part of './max_player.dart';

class _TrackName extends StatelessWidget {
  const _TrackName({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 21.0, left: 34.0, right: 34.0),
      child: ScopedModelDescendant<PlayerModel>(
          builder: (BuildContext context, Widget child, PlayerModel model) {
        if (model.currentTrack != null) {
          return Text(
            model.currentTrack.name,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.title,
            softWrap: true,
          );
        } else {
          return Container();
        }
      }),
    );
  }
}
