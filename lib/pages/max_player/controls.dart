part of './max_player.dart';

class _Controls extends StatelessWidget {
  const _Controls({Key key}) : super(key: key);

  final Color activeColor = const Color(0xff02212E);
  //260A14
  final double sizePlay = 62.0;
  final double sizeOthers = 55.0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.skip_next),
            color: activeColor,
            iconSize: sizeOthers,
            onPressed: () async {
              Track currentTrack = PlayerModel.of(context).currentTrack;
              Track nextTrack =
                  await PlayListModel.of(context).getAutoNextTrack(currentTrack);

              if (nextTrack != null) {
                PlayerModel.of(context).play(nextTrack);
              } else {
                // stop playing
                PlayerModel.of(context).stop();
                PlayerModel.of(context).currentTrack = null;
              }

              MaxPlayerModel.of(context).reset();
            },
          ),
          IconButton(
            icon: ScopedModelDescendant<PlayerModel>(
              builder: (BuildContext context, Widget child, PlayerModel model) {
                // if (model.audioPlayer.state == AudioPlayerState.COMPLETED) {
                //   _autoPlayNext(context);
                // }
                if (model.isPlaying)
                  return Icon(Icons.pause);
                else
                  return Icon(Icons.play_arrow);
              },
            ),
            color: activeColor,
            iconSize: sizePlay,
            onPressed: () {
              var player = PlayerModel.of(context);
              if (player.isPlaying)
                player.pause();
              else
                player.resume();
            },
          ),
          IconButton(
            icon: Icon(Icons.skip_previous),
            color: activeColor,
            iconSize: sizeOthers,
            onPressed: () {
              Track currentTrack = PlayerModel.of(context).currentTrack;
              Track prevTrack =
                  PlayListModel.of(context).getPreviousTrack(currentTrack);

              if (prevTrack != null) {
                PlayerModel.of(context).play(prevTrack);
              } else {
                // stop playing
                PlayerModel.of(context).stop();
                PlayerModel.of(context).currentTrack = null;
              }

              MaxPlayerModel.of(context).reset();
            },
          ),
        ],
      ),
    );
  }
}
