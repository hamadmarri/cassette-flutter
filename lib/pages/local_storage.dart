import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:cassette/reusables/colorful_background.dart';

class LocalStoragePage extends StatelessWidget {
  LocalStoragePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: LocalStorageScaffold(title: "كاسيت"),
    );
  }
}

class LocalStorageScaffold extends StatefulWidget {
  const LocalStorageScaffold({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LocalStorageScaffold createState() => _LocalStorageScaffold();
}

class _LocalStorageScaffold extends State<LocalStorageScaffold> {
  final TextEditingController textCtrl = TextEditingController();

  String message = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      // appBar: TransparentAppBar.appBar(widget.title),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: textCtrl,
            ),
            RaisedButton(
              child: Text("store"),
              onPressed: _store,
            ),
            RaisedButton(
              child: Text("retrieve"),
              onPressed: _retrieve,
            ),
            Text(
              message,
              style: Theme.of(context).textTheme.headline,
            ),
          ],
        ),
      ),
    );
  }

  Future _store() async {
    final String textToStore = textCtrl.text;
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString("test", textToStore);

    setState(() {
      message = "Strored";
    });
  }

  Future _retrieve() async {
    var prefs = await SharedPreferences.getInstance();
    final String ret = prefs.getString("test");
    setState(() {
      message = ret;
    });
  }
}
