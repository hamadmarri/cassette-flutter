import 'package:cassette/BackendAPI/artistsAPI.dart';
import 'package:cassette/modules/modules.dart';
import 'package:cassette/reusables/artist_item.dart';
import 'package:cassette/reusables/bottom_bar.dart';
import 'package:cassette/reusables/colorful_background.dart';
import 'package:cassette/reusables/fetch_loading.dart';
import 'package:cassette/reusables/mini_player.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ArtistsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: ScopedModel<FetchLoadingModel>(
        model: FetchLoadingModel()..initScrollController(),
        child: Container(
          decoration: ColorfulBackground(),
          child: _Content(),
        ),
      ),
    );
  }
}

class _Content extends StatefulWidget {
  const _Content({Key key}) : super(key: key);

  @override
  _ContentState createState() => _ContentState();
}

class _ContentState extends State<_Content> {
  ArtistsAPI artistsAPI = new ArtistsAPI();
  Artists artistsCtrl;
  List<Artist> artists = new List<Artist>();

  @override
  void initState() {
    FetchLoadingModel.of(context).initCallback(_getMoreArtists);
    _getArtists();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(),
      body: Stack(
        children: <Widget>[
          Center(
            child: SingleChildScrollView(
              controller: FetchLoadingModel.of(context).scrollController,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 200.0),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  direction: Axis.horizontal,
                  children: artists.map((a) {
                    return Padding(
                      /// padding 7 in ios was good
                      /// but my oppo phone shows only on column
                      padding: const EdgeInsets.all(0.0),
                      child: ArtistItem(artist: a),
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 300.0,
            right: (MediaQuery.of(context).size.width / 2) - 25,
            child: buildLoadingIndicator(),
          ),
          new MiniPlayer(),
          new BottomBar(page: NavPage.others),
        ],
      ),
    );
  }

  ScopedModelDescendant<FetchLoadingModel> buildLoadingIndicator() {
    return ScopedModelDescendant<FetchLoadingModel>(
      builder: (context, child, model) {
        return model.loadingIndicator();
      },
    );
  }

  void _getArtists() async {
    FetchLoadingModel.of(context).startLoading();
    artistsCtrl = await artistsAPI.fetch();

    setState(() {
      artists.addAll(artistsCtrl.results);
    });

    _getMoreArtists();

    FetchLoadingModel.of(context).finishLoading();
  }

  _getMoreArtists() async {
    // fetch more if next page
    if (artistsCtrl.next == null) return;

    artistsCtrl = await artistsAPI.fetchMore(artistsCtrl.next);

    setState(() {
      artists.addAll(artistsCtrl.results);
    });
  }
}
