// import 'package:admob_flutter/admob_flutter.dart';
import 'package:cassette/BackendAPI/artistsAPI.dart';
import 'package:cassette/modules/modules.dart';
import 'package:cassette/reusables/bottom_bar.dart';
import 'package:cassette/reusables/colorful_background.dart';
import 'package:cassette/reusables/fetch_loading.dart';
import 'package:cassette/reusables/mini_player.dart';
import 'package:cassette/reusables/track_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';

class ArtistPage extends StatelessWidget {
  ArtistPage({Key key, this.artist}) : super(key: key);

  final Artist artist;

  @override
  Widget build(BuildContext context) {
    return ScopedModel<FetchLoadingModel>(
      model: FetchLoadingModel()..initScrollController(),
      child: Container(
        decoration: ColorfulBackground(),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            children: <Widget>[
              new ArtistHeader(artist: artist),
              _TracksList(key, artist),
            ],
          ),
        ),
      ),
    );
  }
}

class ArtistHeader extends StatelessWidget {
  const ArtistHeader({
    Key key,
    @required this.artist,
  }) : super(key: key);

  final Artist artist;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.25,
          child: Image.network(
            artist.file,
            fit: BoxFit.fitWidth,
          ),
        ),
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          child: Container(
            color: Colors.black.withOpacity(0.34),
          ),
        ),
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: AppBar(
            iconTheme: IconThemeData(color: Colors.white),
          ),
        ),
        Positioned(
          bottom: 10,
          right: 20,
          left: 20,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                artist.name,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                ),
              ),
              Flexible(
                fit: FlexFit.loose,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "عدد الاغاني: ${artist.trackCount}",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      artist.country,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class _TracksList extends StatefulWidget {
  const _TracksList(Key key, this.artist) : super(key: key);

  final Artist artist;
  @override
  _TracksListState createState() => _TracksListState();
}

class _TracksListState extends State<_TracksList> {
  ArtistsAPI artistsAPI = new ArtistsAPI();
  Tracks tracksCtrl;
  List<Track> tracks = [];
  final compact = NumberFormat.compact();

  @override
  void initState() {
    FetchLoadingModel.of(context).initCallback(_getMoreTracks);

    _getTracks();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Flexible(
                child: ListView.builder(
                    controller: FetchLoadingModel.of(context).scrollController,
                    itemCount: tracks.length + 1,
                    itemBuilder: (context, i) {
                      if (i == tracks.length) return SizedBox(height: 180.0);
                      return TrackItem(track: tracks[i]);
                    }),
              ),
            ],
          ),
          Positioned(
            bottom: 300.0,
            right: (MediaQuery.of(context).size.width / 2) - 25,
            child: buildLoadingIndicator(),
          ),
          new MiniPlayer(),
          new BottomBar(page: NavPage.others),
        ],
      ),
    );
  }

  ScopedModelDescendant<FetchLoadingModel> buildLoadingIndicator() {
    return ScopedModelDescendant<FetchLoadingModel>(
      builder: (context, child, model) {
        return model.loadingIndicator();
      },
    );
  }

  void _getTracks() async {
    FetchLoadingModel.of(context).startLoading();

    tracksCtrl = await artistsAPI.fetchTracks(widget.artist.id);

    // assign tracksCtrl to each track in it
    // to help playlist to handle next and previous
    tracksCtrl.results.forEach((t) {
      return t.playlistMeta = new PlaylistMeta(widget.artist.id,
          "getRandomTracksOfArtist", tracksCtrl, TrackPlaylistType.TRACKS);
      // return t.tracksCtrl = tracksCtrl;
    });

    setState(() {
      // tracks = tracksCtrl.results;
      tracks.addAll(tracksCtrl.results);
    });

    FetchLoadingModel.of(context).finishLoading();

    _getMoreTracks();
  }

  _getMoreTracks() async {
    if (tracksCtrl.next == null) return;

    tracksCtrl = await artistsAPI.fetchMoreTracks(tracksCtrl.next);

    // assign tracksCtrl to each track in it
    // to help playlist to handle next and previous
    // tracksCtrl.results.forEach((t) => t.tracksCtrl = tracksCtrl);
    tracksCtrl.results.forEach((t) {
      return t.playlistMeta = new PlaylistMeta(widget.artist.id,
          "getRandomTracksOfArtist", tracksCtrl, TrackPlaylistType.TRACKS);
    });

    setState(() {
      tracks.addAll(tracksCtrl.results);
    });
  }
}
