import 'dart:ui';

// import 'package:admob_flutter/admob_flutter.dart';
import 'package:cassette/BackendAPI/authAPI.dart';
import 'package:cassette/modules/play_list.dart';
import 'package:cassette/navigator.dart';
import 'package:cassette/pages/artists_page.dart';
import 'package:cassette/pages/signup.dart';
import 'package:cassette/pages/tracks_list.dart';
import 'package:cassette/reusables/artist_item.dart';
import 'package:cassette/reusables/bottom_bar.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import 'package:cassette/modules/player.dart';

import 'package:cassette/BackendAPI/tracksAPI.dart';
import 'package:cassette/BackendAPI/artistsAPI.dart';

import 'package:cassette/reusables/colorful_background.dart';
import 'package:cassette/reusables/mini_player.dart';

import '../modules/modules.dart';

class LandingPage extends StatelessWidget {
  LandingPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: LandingScaffold(),
    );
  }
}

class LandingScaffold extends StatefulWidget {
  const LandingScaffold({Key key}) : super(key: key);

  @override
  _LandingScaffold createState() => _LandingScaffold();
}

class _LandingScaffold extends State<LandingScaffold> {
  TracksAPI tracksAPI = new TracksAPI();
  ArtistsAPI artistsAPI = new ArtistsAPI();
  Tracks tracksCtrl;
  Tracks downloadedTracksCtrl;
  Artists artistsCtrl;
  List<Track> mostListenedTracks = [];
  List<Track> downloadedTracks = [];
  List<Artist> famousArtists = [];
  List<Track> randomTracks = [];
  final compact = NumberFormat.compact();

  final double itesHeight = 260.0;
  final double itesWidth = 260.0 * 0.618;
  double itemImageHeight = 260.0 * 0.618;

  int isLoading = 4;

  @override
  initState() {
    _fetchTracks();
    _fetchArtists();
    _fetchDownloadedTracks();
    _fetchRandomTracks();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: TransparentAppBar.appBar(''),
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 13.0),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // RaisedButton(
                  //   child: Text("clear storage"),
                  //   onPressed: () {
                  //     Storage.clear();
                  //   },
                  // ),
                  _buildHeaderImage(context),
                  // isLoading > 0 ? new Lo() : Container(),
                  ItemSideList(
                    itesHeight: itesHeight,
                    context: context,
                    title: "مختارات",
                    list: randomTracks,
                    builder: _buildRandomTrack,
                    tracksCtrl: randomTracks.isNotEmpty
                        ? randomTracks[0].playlistMeta.tracksCtrl
                        : null,
                  ),
                  ItemSideList(
                    itesHeight: itesHeight,
                    context: context,
                    title: "فنانين",
                    list: famousArtists,
                    builder: _buildFamousArtist,
                    artistsCtrl: artistsCtrl,
                  ),
                  ItemSideList(
                    itesHeight: itesHeight,
                    context: context,
                    title: "حُملت للتو",
                    list: downloadedTracks,
                    builder: _buildDownloadedTrack,
                    tracksCtrl: downloadedTracksCtrl,
                  ),
                  ItemSideList(
                    itesHeight: itesHeight,
                    context: context,
                    title: "الاكثر استماعا",
                    list: mostListenedTracks,
                    builder: _buildMostListenedTrack,
                    tracksCtrl: mostListenedTracks.isNotEmpty
                        ? mostListenedTracks[0].playlistMeta.tracksCtrl
                        : null,
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 60.0),
                      child: AuthAPI.isGuest
                          ? RaisedButton(
                              color: Colors.blueAccent,
                              child: Text(
                                "تسجيل",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                goTo(context, SignupPage());
                              },
                            )
                          : Container(),
                    ),
                  ),
                  SizedBox(
                    height: 200,
                  ),
                ],
              ),
            ),
          ),
          new MiniPlayer(),
          new BottomBar(page: NavPage.home),
        ],
      ),
    );
  }

  Widget _buildHeaderImage(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 47.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(21.0),
        child: Stack(
          children: <Widget>[
            SizedBox(
              height: 138.0,
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                "assets/images/sea.jpg",
                fit: BoxFit.none,
                scale: 1.6,
                alignment: Alignment.bottomCenter,
              ),
            ),
            Container(
              height: 138.0,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.black38, Colors.transparent],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  stops: [0.0, 0.62],
                ),
              ),
            ),
            Positioned(
              bottom: 10.0,
              left: 18.0,
              child: Text(
                "الموسيقى لا تؤذي احدا",
                style: TextStyle(color: Colors.white, fontFamily: "Aref"),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildMostListenedTrack(BuildContext context, int index) {
    return new TrackContainer(
        itesWidth: itesWidth,
        itemImageHeight: itemImageHeight,
        track: mostListenedTracks[index]);
  }

  Widget _buildFamousArtist(BuildContext context, int index) {
    return ArtistItem(artist: famousArtists[index]);
  }

  Widget _buildDownloadedTrack(BuildContext context, int index) {
    return TrackContainer(
        itesWidth: itesWidth,
        itemImageHeight: itemImageHeight,
        track: downloadedTracks[index]);
  }

  Widget _buildRandomTrack(BuildContext context, int index) {
    return TrackContainer(
        itesWidth: itesWidth,
        itemImageHeight: itemImageHeight,
        track: randomTracks[index]);
  }

  void _fetchTracks() async {
    tracksCtrl = await tracksAPI.fetch();

    // assign tracksCtrl to each track in it
    // to help playlist to handle next and previous
    // tracksCtrl.results.forEach((t) => t.tracksCtrl = tracksCtrl);

    tracksCtrl.results.forEach((t) {
      return t.playlistMeta = new PlaylistMeta(
          -1, "getRandomTrack", tracksCtrl, TrackPlaylistType.TRACKS);
    });

    setState(() {
      // mostListenedTracks = tracksCtrl.results;
      mostListenedTracks.addAll(tracksCtrl.results);
      isLoading--;
    });
  }

  void _fetchDownloadedTracks() async {
    downloadedTracksCtrl = await tracksAPI.fetchDownloaded();

    // assign tracksCtrl to each track in it
    // to help playlist to handle next and previous
    downloadedTracksCtrl.results.forEach((t) {
      return t.playlistMeta = new PlaylistMeta(-1, "getRandomTrack",
          downloadedTracksCtrl, TrackPlaylistType.DOWNLOADED_LIST);
    });

    setState(() {
      // downloadedTracks = downloadedTracksCtrl.results;
      downloadedTracks.addAll(downloadedTracksCtrl.results);
      isLoading--;
    });
  }

  void _fetchRandomTracks() async {
    Tracks randomCtrl = await tracksAPI.fetchRandom10();

    randomCtrl.results.forEach((t) {
      return t.playlistMeta = new PlaylistMeta(
          -1, "getRandomTrack", randomCtrl, TrackPlaylistType.RANDOM);
    });

    setState(() {
      randomTracks.addAll(randomCtrl.results);
    });

    randomCtrl = await tracksAPI.fetchRandom10();

    randomCtrl.results.forEach((t) {
      return t.playlistMeta = new PlaylistMeta(
          -1, "getRandomTrack", randomCtrl, TrackPlaylistType.RANDOM);
    });

    setState(() {
      randomTracks.addAll(randomCtrl.results);
      isLoading--;
    });
  }

  void _fetchArtists() async {
    artistsCtrl = await artistsAPI.fetch();
    PlayListModel.of(context).artistsCount = artistsCtrl.count;
    setState(() {
      famousArtists = artistsCtrl.results;
      isLoading--;
    });
  }
}

class ItemSideList extends StatelessWidget {
  const ItemSideList({
    Key key,
    @required this.itesHeight,
    @required this.context,
    @required this.title,
    @required this.list,
    @required this.builder,
    this.tracksCtrl,
    this.artistsCtrl,
  }) : super(key: key);

  final double itesHeight;
  final BuildContext context;
  final String title;
  final List list;
  final Function builder;
  final Tracks tracksCtrl;
  final Artists artistsCtrl;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: <Widget>[
        Divider(),
        ListTile(
          leading: Text(
            title,
            style: Theme.of(context).textTheme.headline,
          ),
          trailing: GestureDetector(
            onTap: () {
              if (tracksCtrl != null) {
                goTo(context, TracksList(tracksCtrl: tracksCtrl));
              } else if (artistsCtrl != null) {
                goTo(context, ArtistsPage());
              }
            },
            child: Text(
              "المزيد",
              style: TextStyle(
                color: Color(0xff0388A6),
                decoration: TextDecoration.underline,
              ),
            ),
          ),
        ),
        Container(
          height: itesHeight,
          margin: EdgeInsets.only(top: 13.0, bottom: 21.0),
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: list.length,
            itemBuilder: builder,
          ),
        ),
      ],
    );
  }
}

class TrackContainer extends StatelessWidget {
  const TrackContainer({
    Key key,
    @required this.itesWidth,
    @required this.itemImageHeight,
    @required this.track,
  }) : super(key: key);

  final double itesWidth;
  final double itemImageHeight;
  final Track track;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        PlayerModel.of(context).play(track);
        PlayListModel.of(context).restPlaylist();
      },
      child: Container(
        margin: EdgeInsets.only(left: 8.0),
        width: itesWidth,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(21.0),
          child: Card(
            child: Stack(
              children: <Widget>[
                SizedBox(
                  height: itemImageHeight,
                  width: itesWidth,
                  child: Image.network(
                    track.artists[0].file,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  top: itemImageHeight + 8.95143523,
                  right: 0.0,
                  left: 0.0,
                  child: SizedBox(
                    width: itesWidth,
                    child: ListTile(
                      title: Text(
                        track.name,
                        overflow: TextOverflow.ellipsis,
                      ),
                      subtitle: Text(
                        track.artists[0].name,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
