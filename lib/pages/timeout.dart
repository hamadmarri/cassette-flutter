import 'package:cassette/modules/ads.dart';
import 'package:cassette/modules/limit.dart';
import 'package:cassette/reusables/colorful_background.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class TimeoutPage extends StatelessWidget {
  TimeoutPage({Key key}) : super(key: key) {
    Limit.navigated = true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: TimeoutScaffold(),
    );
  }
}

class TimeoutScaffold extends StatefulWidget {
  @override
  _TimeoutScaffoldState createState() => _TimeoutScaffoldState();
}

class _TimeoutScaffoldState extends State<TimeoutScaffold> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: WillPopScope(
        onWillPop: () async => false,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(top: 100.0, left: 40.0, right: 40.0),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 21.0),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.46,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(13.0),
                      child: Image.asset("assets/images/icon_logo_ios.png"),
                    ),
                  ),
                ),
                ScopedModelDescendant<AdsModel>(
                  builder:
                      (BuildContext context, Widget child, AdsModel model) {
                    if (!model.rewarded) {
                      return Column(
                        children: <Widget>[
                          Text(
                            "عذرا, لقد استمعت ل 25 دقيقة\nيمكنك الاستماع ل 25 دقيقة اخرى بعد مشاهدة الاعلان",
                            style: Theme.of(context).textTheme.subhead,
                            textAlign: TextAlign.center,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 21.0),
                            child: RaisedButton(
                              child: Text("شاهد الاعلان"),
                              color: Colors.blueAccent,
                              textColor: Colors.white,
                              onPressed: () {
                                AdsModel.of(context).showReward();
                              },
                            ),
                          ),
                        ],
                      );
                    } else {
                      model.rewarded = false;
                      Limit.refill();
                      Limit.navigated = false;
                      return RaisedButton(
                        child: Text("عودة"),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
