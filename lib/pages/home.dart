import 'package:cassette/reusables/track_item.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import 'package:cassette/BackendAPI/tracksAPI.dart';
import 'package:cassette/reusables/colorful_background.dart';
import 'package:cassette/reusables/fetch_loading.dart';
import 'package:cassette/reusables/mini_player.dart';
import 'package:cassette/reusables/transparent_appbar.dart';

import '../modules/modules.dart';

class Home extends StatelessWidget {
  Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ColorfulBackground(),
      child: ScopedModel<FetchLoadingModel>(
        model: FetchLoadingModel()..initScrollController(),
        child: HomeScaffold(title: "كاسيت"),
      ),
    );
  }
}

class HomeScaffold extends StatefulWidget {
  const HomeScaffold({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeScaffold createState() => _HomeScaffold();
}

class _HomeScaffold extends State<HomeScaffold> {
  TracksAPI tracksAPI = new TracksAPI();
  Tracks tracksCtrl;
  // Rest rest = Rest();
  List<Track> tracks = [];
  final compact = NumberFormat.compact();

  @override
  void initState() {
    FetchLoadingModel.of(context).initCallback(_getMoreTracks);

    _getTracks();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Flexible(
                child: ListView.builder(
                    controller: ScopedModel.of<FetchLoadingModel>(context)
                        .scrollController,
                    itemCount: tracks.length,
                    itemBuilder: (context, i) {
                      return TrackItem(track: tracks[i]);
                    }),
              ),
              buildLoadingIndicator(),
            ],
          ),
          MiniPlayer(),
          Positioned(
            top: 0.0,
            left: 0.0,
            right: 0.0,
            child: TransparentAppBar.appBar(widget.title),
          )
        ],
      ),
      floatingActionButton: buildGoToTopButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }

  ScopedModelDescendant<FetchLoadingModel> buildLoadingIndicator() {
    return ScopedModelDescendant<FetchLoadingModel>(
      builder: (context, child, model) {
        return model.loadingIndicator();
      },
    );
  }

  buildGoToTopButton() {
    return ScopedModelDescendant<FetchLoadingModel>(
      builder: (context, child, model) {
        debugPrint("buildGoToTopButton()");
        return model.showGoToTop ? model.showGoToTopButton() : Container();
      },
    );
  }

  void _getTracks() async {
    FetchLoadingModel.of(context).startLoading();

    tracksCtrl = await tracksAPI.fetch();
    setState(() {
      tracks = tracksCtrl.results;
    });

    _getMoreTracks();

    FetchLoadingModel.of(context).finishLoading();
  }

  _getMoreTracks() async {
    tracksCtrl = await tracksAPI.fetchMore(tracksCtrl.next);

    setState(() {
      tracks.addAll(tracksCtrl.results);
    });
  }
}
