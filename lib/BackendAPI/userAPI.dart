import 'dart:async';
import 'dart:convert';

import 'package:cassette/BackendAPI/rest.dart';
import 'package:cassette/modules/modules.dart';

class UserAPI extends Rest {
  static UserDetails userDetails;

  Future fetchUserDetails() async {
    if (userDetails == null) {
      var response = await get(path: "userdetails");
      Map pageMap = json.decode(utf8.decode(response.bodyBytes));
      userDetails = UserDetails.fromJson(pageMap['results'][0]);
    }
  }

  Future addToFavorite(int trackId) async {
    Map data = {"track": trackId};

    var response = await post(path: "addtofavorite", data: data);
    // print(response.statusCode); // must be 201
    return response;
  }

  Future removeFromFavorite(int trackId) async {
    Map data = {"track": trackId};

    var response = await post(path: "removefromfavorite", data: data);
    // print(response.statusCode); // must be 201
    return response;
  }

  Future fetchFaveSongs() async {
    int uid = 0;

    if (userDetails != null) uid = userDetails.id;

    var response = await get(path: "kasaitusers/$uid/favoritesongs");

    if (response.statusCode == 200) {
      Map pageMap = json.decode(utf8.decode(response.bodyBytes));
      return Tracks.fromJson(pageMap);
    } else {
      return new Tracks(0, null, null, List<Track>(0));
    }
  }
}
