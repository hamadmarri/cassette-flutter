import 'dart:convert';

import 'package:cassette/BackendAPI/rest.dart';
import 'package:cassette/modules/modules.dart';

class TracksAPI extends Rest {
  Future fetch() async {
    var response = await get(path: "tracks");
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Tracks.fromJson(pageMap);
  }

  Future fetchMore(String path) async {
    var response = await get(fullPath: path);
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Tracks.fromJson(pageMap);
  }

  Future fetchDownloaded() async {
    var response = await get(path: "downloaded");
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));

    // downloaded has extra id as downloaded id, need to remap
    List resultsList = pageMap["results"];
    List<Track> newResults =
        resultsList.map((d) => Track.fromJson(d['track'])).toList();

    Tracks tracks = new Tracks(
      pageMap["count"] as int,
      pageMap["next"] as String,
      pageMap["previous"] as String,
      newResults,
    );

    return tracks;
  }

  Future fetchMoreDownloaded(String path) async {
    var response = await get(fullPath: path);
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));

    // downloaded has extra id as downloaded id, need to remap
    List resultsList = pageMap["results"];
    List<Track> newResults =
        resultsList.map((d) => Track.fromJson(d['track'])).toList();

    Tracks tracks = new Tracks(
      pageMap["count"] as int,
      pageMap["next"] as String,
      pageMap["previous"] as String,
      newResults,
    );

    return tracks;
  }

  Future fetchRandom10() async {
    var response = await get(path: "randomtrack");
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Tracks.fromJson(pageMap);
  }

  Future search(String value) async {
    var response = await get(
      path: "tracks",
      parameter: "search",
      parameterValue: value,
    );
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Tracks.fromJson(pageMap);
  }

  Future genericSearch(String value) async {
    var response = await get(
      path: "search",
      parameter: "search",
      parameterValue: value,
    );
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Tracks.fromJson(pageMap);
  }

  Future listen(int trackId) async {
    Map data = {"track": trackId};
    var response = await post(path: "listen", data: data);
    // print(response.statusCode); // must be 201
    return response;
  }

  Future createDownload(int trackId) async {
    Map data = {"track": trackId};
    var response = await post(path: "createdownload", data: data);
    // print(response.statusCode); // must be 201
    return response;
  }

  Future createFeedback(String text) async {
    Map data = {"text": text};
    var response = await post(path: "createfeedback", data: data);
    // print(response.statusCode); // must be 201
    return response;
  }
}
