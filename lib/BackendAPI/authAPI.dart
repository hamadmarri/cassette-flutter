import 'dart:convert';

import 'package:cassette/BackendAPI/rest.dart';
import 'package:cassette/modules/storage.dart';
import 'package:http/http.dart';

class AuthAPI extends Rest {
  static bool isLoggedIn = false;
  static bool isGuest = false;

  static Future<bool> checkLogin() {
    // load token from storage
    return Storage.retrieveString('token').then((token) {
      // if not empty, set rest.token,
      // islogged to true in and return true
      if (token != null) {
        print(token);
        isLoggedIn = true;

        // set token
        Rest.token = token;
        return true;
      }
      // if empty load user user credentials from storage
      else {
        return false;
      }
    });
  }

  Future<String> login(String username, String password) async {
    Map usercredentials = {"username": username, "password": password};
    isGuest = false;

    // set flage newUser to false
    await Storage.storeString("newUser", "f");

    return await _getToken(usercredentials);
  }

  Future<String> loginGuest() async {
    Map usercredentials = {"username": "guest", "password": "guest1234"};
    isGuest = true;
    return await _getToken(usercredentials);
  }

  Future<String> _getToken(Map usercredentials) async {
    print('AuthAPI.getToken(): getting token');
    return super
        .post(data: usercredentials, path: "api-token-auth")
        .then((Response response) {
      if (response.statusCode == 200) {
        Map pageMap = json.decode(utf8.decode(response.bodyBytes));
        String token = pageMap["token"];
        print(token);

        // set token
        Rest.token = token;

        // store token
        Storage.storeString('token', token);

        return null;
      } else {
        print(
            "AuthAPI.getToken() ERROR ${response.statusCode}: ${utf8.decode(response.bodyBytes)}");
        Map pageMap = json.decode(utf8.decode(response.bodyBytes));
        return pageMap["non_field_errors"][0];
      }
    });
  }

  Future<String> signup({
    username = '',
    password = '',
    lastname = '',
    firstname = '',
    bio = '',
    gender = '0',
    avatar,
  }) async {
    print('AuthAPI.signup(): signup');
    Map newUser = {
      "username": username,
      "password": password,
      // "lastname": lastname,
      "firstname": firstname,
      "bio": bio,
      "gender": gender,
      // "avatar": avatar,
    };

    return super
        .post(data: newUser, path: "kasaitusers")
        .then((Response response) async {
      print(response.statusCode);
      print(utf8.decode(response.bodyBytes));

      /// if status code 500
      /// user is already existed

      if (response.statusCode == 500) {
        return 'تعذر التسجيل ربما لان الايميل مسجل مسبقا';
      }

      // set flage newUser to false
      await Storage.storeString("newUser", "f");

      return null;
    });
  }
}
