import 'dart:convert';

import 'package:cassette/BackendAPI/rest.dart';
import 'package:cassette/modules/modules.dart';

class ArtistsAPI extends Rest {
  Future fetch() async {
    var response = await get(path: "artists");
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Artists.fromJson(pageMap);
  }

  Future fetchMore(String path) async {
    var response = await get(fullPath: path);
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Artists.fromJson(pageMap);
  }

  Future search(String value) async {
    var response = await get(
      path: "artists",
      parameter: "search",
      parameterValue: value,
    );
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Artists.fromJson(pageMap);
  }

  Future fetchTracks(id) async {
    var response = await get(path: "artists/$id/tracks");
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Tracks.fromJson(pageMap);
  }

  Future fetchMoreTracks(String path) async {
    var response = await get(fullPath: path);
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Tracks.fromJson(pageMap);
  }

  Future fetchRandomTrack(id) async {
    var response = await get(path: "artists/$id/tracks/random");
    Map pageMap = json.decode(utf8.decode(response.bodyBytes));
    return Tracks.fromJson(pageMap);
  }
}
