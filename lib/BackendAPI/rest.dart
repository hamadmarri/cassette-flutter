import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class Rest {
  static String _apiUrl = 'http://165.22.67.206/kasait';
  static String streamUrl = "http://167.86.112.231/music/";
  static String streamUrlSecure = "https://kasait.xyz/music/";
  static String token;

  static Map<String, String> _getHeaders() {
    if (token != null) {
      return {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader:
            // 'Token e984595e08a1cd834cff9cbd03ce7ec526b18e99'
            'Token $token'
      };
    } else {
      return {
        HttpHeaders.contentTypeHeader: 'application/json',
      };
    }
  }

  get({
    String path = "",
    String fullPath = "",
    String parameter = "",
    String parameterValue = "",
  }) async {
    if (fullPath.isEmpty) {
      if (parameter.isEmpty) {
        return http.get('$_apiUrl/$path/', headers: _getHeaders());
      } else {
        return http.get('$_apiUrl/$path/?$parameter=$parameterValue',
            headers: _getHeaders());
      }
    } else {
      return http.get('$fullPath', headers: _getHeaders());
    }
  }

  Future<http.Response> post({
    String path = "",
    Map data,
  }) async {
    return http.post('$_apiUrl/$path/',
        body: jsonEncode(data), headers: _getHeaders());
  }
}
