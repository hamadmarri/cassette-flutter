import 'package:cassette/config.dart';
import 'package:cassette/modules/ads.dart';
import 'package:cassette/modules/play_list.dart';
import 'package:cassette/modules/player.dart';
import 'package:cassette/pages/splash.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

// import 'package:cassette/pages/index.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:wakelock/wakelock.dart';

// import 'package:admob_flutter/admob_flutter.dart';

void main() {
  runApp(CassetteApp());

  if (Config.SHOW_ADS) {
    FirebaseAdMob.instance.initialize(appId: Config.ADMOB_APP_ID);
    debugPrint("FirebaseAdMob.instance.initialize");
  }

  // keep screen awake
  Wakelock.enable();
  debugPrint("Wakelock.enable");
}

class CassetteApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<PlayerModel>(
      model: PlayerModel(),
      child: ScopedModel<PlayListModel>(
        model: PlayListModel(),
        child: ScopedModel<AdsModel>(
          model: AdsModel()..init(),
          child: MaterialApp(
            debugShowCheckedModeBanner: Config.IS_DEBUG,
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: [
              Locale("ar"),
            ],
            locale: Locale("ar"),
            title: 'كاسيت',
            theme: buildThemeData(),
            home: SplashPage(),
          ),
        ),
      ),
    );
  }

  ThemeData buildThemeData() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.transparent,
        iconTheme: IconThemeData(
          color: Color(0xff260A14),
        ),
        elevation: 0.0,
      ),
      // textTheme: TextTheme(
      //   body1: TextStyle(color: Color(0xff03658C)),
      //   body2: TextStyle(color: Color(0xff03658C)),
      //   headline: TextStyle(color: Color(0xff03658C)),
      //   subhead: TextStyle(color: Color(0xff03658C)),
      //   // caption: TextStyle(color: Color(0xff260A14)),
      //   display1: TextStyle(color: Color(0xff03658C)),

      // ),

      // backgroundColor: Color(0xfff2fbff),
      // cardColor: Color(0xfff2fbff),
      cardTheme: CardTheme(
        elevation: 0.34,
      ),
    );
  }
}
