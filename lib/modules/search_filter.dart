class SearchFilter {
  static filter(String search) {
    search = replaceArabic(search);
    search = whitelist(search);
    search = replaceEnglish(search);
    search = replaceWords(search);
    search = replaceAbdul(search);

    return search;
  }

  static String replaceArabic(String search) {
    String s = search;

    s = s.replaceAll('أ', 'ا');
    s = s.replaceAll('إ', 'ا');
    s = s.replaceAll('آ', 'ا');

    return s;
  }

  static String whitelist(String search) {
    String s = search;
    String domain =
        "ذضصثقفغعهخحجدشسيبلاتنمكطئءؤرﻻىةوزظ ()abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    s = s.replaceAll('أ', 'ا');
    s = s.replaceAll('إ', 'ا');
    s = s.replaceAll('آ', 'ا');

    for (int i = 0; i < s.length; i++) {
      if (domain.indexOf(s[i]) == -1) {
        s = s.replaceAll(s[i], '');
        i--;
      }
    }

    return s;
  }

  static String replaceEnglish(String search) {
    String s = search;

    s = s.toLowerCase();

    s = s.replaceAll('sh', 'ش');
    s = s.replaceAll('kh', 'خ');
    s = s.replaceAll('ae', 'ائ');

    s = s.replaceAll('a', 'ا');
    s = s.replaceAll('b', 'ب');
    s = s.replaceAll('w', 'و');
    s = s.replaceAll('t', 'ت');
    s = s.replaceAll('s', 'س');
    s = s.replaceAll('k', 'ك');
    s = s.replaceAll('th', 'ذ');
    s = s.replaceAll('y', 'ي');
    s = s.replaceAll('e', 'ي');
    s = s.replaceAll('i', 'ي');
    
    s = s.replaceAll('d', 'د');
    s = s.replaceAll('f', 'ف');
    s = s.replaceAll('g', 'ج');
    s = s.replaceAll('h', 'ه');
    s = s.replaceAll('j', 'ج');
    s = s.replaceAll('k', 'ك');
    s = s.replaceAll('l', 'ل');
    s = s.replaceAll('m', 'م');
    s = s.replaceAll('n', 'ن');

    s = s.replaceAll('o', 'و');
    s = s.replaceAll('q', 'ق');
    s = s.replaceAll('r', 'ر');
    s = s.replaceAll('z', 'ز');
    return s;
  }

  static String replaceWords(String search) {
    String s = search;

    s = s.replaceAll('اغاني', '');
    s = s.replaceAll('اغانى', '');

    s = s.replaceAll(RegExp(r'يمني[هة]'), 'اليمن');
    s = s.replaceAll(RegExp(r'كويتي[هة]'), 'الكويت');
    s = s.replaceAll(RegExp(r'عراقي[هة]'), 'العراق');
    s = s.replaceAll(RegExp(r'سعودي[هة]'), 'السعودية');
    s = s.replaceAll(RegExp(r'سوري[هة]'), 'سوريا');
    s = s.replaceAll(RegExp(r'مصري[هة]'), 'مصر');
    s = s.replaceAll(RegExp(r'لبناني[هة]'), 'لبنان');
    s = s.replaceAll(RegExp(r'اردني[هة]'), 'الاردن');
    s = s.replaceAll(RegExp(r'اماراتي[هة]'), 'الامارات');
    s = s.replaceAll(RegExp(r'مغربي[هة]'), 'المغرب');
    s = s.replaceAll(RegExp(r'تونسي[هة]'), 'تونس');
    s = s.replaceAll(RegExp(r'جزائري[هة]'), 'الجزائر');
    s = s.replaceAll(RegExp(r'سوداني[هة]'), 'السودان');
    s = s.replaceAll(RegExp(r'ليبي[هة]'), 'ليبيا');
    s = s.replaceAll(RegExp(r'فلسطيني[هة]'), 'فلسطين');
    s = s.replaceAll(RegExp(r'قطري[هة]'), 'قطر');
    s = s.replaceAll(RegExp(r'بحريني[هة]'), 'البحرين');

    return s;
  }

  static String replaceAbdul(String search) {
    String s = search;
    s = s.replaceAll('عبد ال', 'عبدال');
    return s;
  }

  static String separateLetters(String search) {
    String s = search;

    s = s.replaceAll(" ", "");
    List<String> split = s.split('');
    split = split.toSet().toList();
    s = split.join();
    s = s.replaceAllMapped(RegExp(r'\S'), (match) => ' ${match.group(0)}');

    return s;
  }
}
