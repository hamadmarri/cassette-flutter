// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'modules.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Track _$TrackFromJson(Map<String, dynamic> json) {
  return Track(
      json['id'] as int,
      json['name'] as String,
      (json['artists'] as List)
          ?.map((e) =>
              e == null ? null : Artist.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['trackdetails'] as int,
      json['listened_times'] as int,
      json['link'] as String,
      json['album'] as int,
      json['original_song'] as int);
}

Map<String, dynamic> _$TrackToJson(Track instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'artists': instance.artists,
      'trackdetails': instance.trackdetails,
      'listened_times': instance.listenedTimes,
      'link': instance.link,
      'album': instance.album,
      'original_song': instance.originalSong
    };

Tracks _$TracksFromJson(Map<String, dynamic> json) {
  return Tracks(
      json['count'] as int,
      json['next'] as String,
      json['previous'] as String,
      (json['results'] as List)
          ?.map((e) =>
              e == null ? null : Track.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$TracksToJson(Tracks instance) => <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };

Artist _$ArtistFromJson(Map<String, dynamic> json) {
  return Artist(
      json['id'] as int,
      json['name'] as String,
      json['track_count'] as int,
      json['subscribe_count'] as int,
      json['file'] as String,
      json['country'] as String);
}

Map<String, dynamic> _$ArtistToJson(Artist instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'country': instance.country,
      'track_count': instance.trackCount,
      'subscribe_count': instance.subscribeCount,
      'file': instance.file
    };

Artists _$ArtistsFromJson(Map<String, dynamic> json) {
  return Artists(
      json['count'] as int,
      json['next'] as String,
      json['previous'] as String,
      (json['results'] as List)
          ?.map((e) =>
              e == null ? null : Artist.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$ArtistsToJson(Artists instance) => <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };

Hashtag _$HashtagFromJson(Map<String, dynamic> json) {
  return Hashtag(json['id'] as int, json['name'] as String,
      json['playlist'] as int, json['favorites_count'] as int);
}

Map<String, dynamic> _$HashtagToJson(Hashtag instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'playlist': instance.playlist,
      'favorites_count': instance.favoritesCount
    };

Hashtags _$HashtagsFromJson(Map<String, dynamic> json) {
  return Hashtags(
      json['count'] as int,
      json['next'] as String,
      json['previous'] as String,
      (json['results'] as List)
          ?.map((e) =>
              e == null ? null : Hashtag.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$HashtagsToJson(Hashtags instance) => <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };

UserDetails _$UserDetailsFromJson(Map<String, dynamic> json) {
  return UserDetails(json['id'] as int, json['user'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserDetailsToJson(UserDetails instance) =>
    <String, dynamic>{'id': instance.id, 'user': instance.user};
