import 'package:cassette/config.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class AdsModel extends Model {
  static AdsModel of(BuildContext context) => ScopedModel.of<AdsModel>(context);

  bool rewarded = false;

  MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    childDirected: false,
    testDevices: <String>['12AA714ACE10764F1EA6C9E8705920EE'],
  );

  init() {
    if (!Config.SHOW_ADS) return;

    print("load reward");
    RewardedVideoAd.instance
        .load(adUnitId: Config.REWARD, targetingInfo: targetingInfo);

    RewardedVideoAd.instance.listener =
        (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
      if (event == RewardedVideoAdEvent.loaded) {
        print("ads:reward loaded");
      } else if (event == RewardedVideoAdEvent.rewarded) {
        rewarded = true;
        notifyListeners();
      } else if (event == RewardedVideoAdEvent.closed) {
        print("load reward");
        RewardedVideoAd.instance
            .load(adUnitId: Config.REWARD, targetingInfo: targetingInfo);
      } else if (event == RewardedVideoAdEvent.failedToLoad) {
        debugPrint("---------- FAILED to load ad");
        RewardedVideoAd.instance
            .load(adUnitId: Config.REWARD, targetingInfo: targetingInfo);
      }
    };
  }

  showReward() async {
    if (Config.SHOW_ADS) {
      RewardedVideoAd.instance.show();
    } else {
      rewarded = true;
      notifyListeners();
    }
  }
}
