import 'package:cassette/config.dart';
import 'package:cassette/modules/modules.dart';
import 'package:share/share.dart';

///
/// IOS: itms-apps://itunes.apple.com/app/[appId]
/// Android: market://details?id=[appPackageId]
/// Android: https://play.google.com/store/apps/details?id=[appPackageId]
/// 'itms-apps://itunes.com/apps/hamad.s.almarri@gmail.com'
/// 'itms-apps://itunes.apple.com/app/id1461425223'

class ShareModule {
  static void shareTrack(Track t) {
    String subject = "استمع لاجمل الاغاني عن طريق تطبيق كاسيت";

    Share.share(
      "${t.name} - (${t.artists[0].name})\n${Config.APP_MARKET_ID}",
      subject: subject,
    );
  }
}
