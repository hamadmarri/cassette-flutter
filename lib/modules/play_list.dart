import 'dart:math';

import 'package:cassette/BackendAPI/artistsAPI.dart';
import 'package:cassette/BackendAPI/tracksAPI.dart';
import 'package:cassette/modules/modules.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:cassette/modules/storage.dart';

enum PlayOptions { OFF, REPEATE_ONCE, REPEATE_LIST, SHUFFLE, RADIO }

class PlayListModel extends Model {
  static PlayListModel of(BuildContext context) =>
      ScopedModel.of<PlayListModel>(context);

  PlayListModel() {
    init();
  }

  PlayOptions mode;
  int artistsCount;

  TracksAPI tracksAPI = new TracksAPI();
  int _currentTrackIndex = -1;

  Future init() async {
    int _mode = await Storage.retrieveInt('play_mode');

    if (_mode != null) {
      mode = PlayOptions.values[_mode];
    } else {
      mode = PlayOptions.REPEATE_LIST;

      await Storage.storeInt('play_mode', mode.index);
    }

    notifyListeners();
  }

  void changeMode() {
    // -1 don't consider radio to be
    mode =
        PlayOptions.values[(mode.index + 1) % (PlayOptions.values.length - 1)];
    Storage.storeInt('play_mode', mode.index);
    notifyListeners();
  }

  void changeModeToRadio() {
    mode = PlayOptions.RADIO;
    notifyListeners();
  }

  void changeModeFromRadio() {
    init();
  }

  void restPlaylist() {
    _currentTrackIndex = -1;
  }

  Track getPreviousTrack(Track currentTrack) {
    Tracks tracksCtrl = currentTrack.playlistMeta.tracksCtrl;
    if (mode == PlayOptions.REPEATE_LIST) {
      // find current track index
      if (_currentTrackIndex == -1) {
        for (var i = 0; i < tracksCtrl.results.length; i++) {
          if (tracksCtrl.results[i].id == currentTrack.id) {
            _currentTrackIndex = i;
            break;
          }
        }
      }

      // decrement index by one
      _currentTrackIndex--;

      if (_currentTrackIndex < 0) {
        return null;
      }

      return tracksCtrl.results[_currentTrackIndex];
    }

    return null;
  }

  Future<Track> getAutoNextTrack(Track currentTrack) async {
    if (mode == PlayOptions.REPEATE_LIST) {
      return await _getRepeatList(currentTrack);
    } else if (mode == PlayOptions.REPEATE_ONCE) {
      return currentTrack;
    } else if (mode == PlayOptions.SHUFFLE) {
      if (currentTrack.playlistMeta.playlistType == TrackPlaylistType.RANDOM) {
        return await _getRepeatList(currentTrack);
      }
      return await _getRandom(currentTrack);
    } else if (mode == PlayOptions.RADIO) {
      return await getRadio();
    }

    return null;
  }

  Future<Track> _getRepeatList(Track currentTrack) async {
    Tracks tracksCtrl = currentTrack.playlistMeta.tracksCtrl;
    TrackPlaylistType playlistType = currentTrack.playlistMeta.playlistType;

    // find current track index
    if (_currentTrackIndex == -1) {
      for (var i = 0; i < tracksCtrl.results.length; i++) {
        if (tracksCtrl.results[i].id == currentTrack.id) {
          _currentTrackIndex = i;
          break;
        }
      }
    }

    // increment index by one
    _currentTrackIndex++;

    // go back to 0 if exceeded the array
    if (_currentTrackIndex >= tracksCtrl.results.length) {
      _currentTrackIndex = 0;

      // fetch more if next page
      if (playlistType == TrackPlaylistType.TRACKS) {
        // print("TRACKS");
        if (tracksCtrl.next != null) {
          tracksCtrl = await tracksAPI.fetchMore(tracksCtrl.next);

          // assign tracksCtrl to each track in it
          // to help playlist to handle next and previous
          tracksCtrl.results.forEach((t) {
            return t.playlistMeta = new PlaylistMeta(
                -1, "getRandomTrack", tracksCtrl, TrackPlaylistType.TRACKS);
          });
        }
        // if no more, then auto play must stop
        else {
          return null;
        }
      } else if (playlistType == TrackPlaylistType.RANDOM) {
        // print("RANDOM");
        tracksCtrl = await tracksAPI.fetchRandom10();

        // assign tracksCtrl to each track in it
        // to help playlist to handle next and previous
        tracksCtrl.results.forEach((t) {
          return t.playlistMeta = new PlaylistMeta(
              -1, "getRandomTrack", tracksCtrl, TrackPlaylistType.RANDOM);
        });
      } else if (playlistType == TrackPlaylistType.DOWNLOADED_LIST) {
        // print("DOWNLOADED_LIST");
        if (tracksCtrl.next != null) {
          tracksCtrl = await tracksAPI.fetchMoreDownloaded(tracksCtrl.next);

          // assign tracksCtrl to each track in it
          // to help playlist to handle next and previous
          tracksCtrl.results.forEach((t) {
            return t.playlistMeta = new PlaylistMeta(-1, "getRandomTrack",
                tracksCtrl, TrackPlaylistType.DOWNLOADED_LIST);
          });
        }
        // if no more, then auto play must stop
        else {
          return null;
        }
      }
    } // if _currentTrackIndex >= tracksCtrl.results.length

    // return next track
    return tracksCtrl.results[_currentTrackIndex];
  }

  Future<Track> _getRandom(Track currentTrack) async {
    int id = currentTrack.playlistMeta.id;
    String from = currentTrack.playlistMeta.from;
    TrackPlaylistType playlistType = currentTrack.playlistMeta.playlistType;

    switch (from) {
      case "getRandomTrack":
        TracksAPI tracksAPI = new TracksAPI();
        Tracks tracksCtrl = await tracksAPI.fetchRandom10();
        tracksCtrl.results[0].playlistMeta = currentTrack.playlistMeta;
        return tracksCtrl.results[0];
        break;
      case "getRandomFaveSong":
        return null;
        break;
      case "getRandomTracksOfArtist":
        ArtistsAPI artistsAPI = new ArtistsAPI();
        Tracks tracksCtrl = await artistsAPI.fetchRandomTrack(id);
        tracksCtrl.results[0].playlistMeta = currentTrack.playlistMeta;
        return tracksCtrl.results[0];
        break;
      case "getRandomDownloaded":
        return null;
        break;
    }

    if (playlistType == TrackPlaylistType.DOWNLOADED_DEVICE) {
      Tracks tracksCtrl = currentTrack.playlistMeta.tracksCtrl;
      int r = Random().nextInt(tracksCtrl.results.length);
      return tracksCtrl.results[r];
    }

    return null;
  }

  Future<Track> getRadio() async {
    // artist 5 is غير معروف has no songs
    // artist 11 is deleted from DB
    var notWantedArtists = [5, 11];
    ArtistsAPI artistsAPI = new ArtistsAPI();

    Random r = Random(DateTime.now().millisecondsSinceEpoch);
    int artistId = r.nextInt(artistsCount) + 1;

    // if random is 5 or 11 where there is no songs
    // keep getting another random
    while (notWantedArtists.contains(artistId)) {
      debugPrint("############### bad artist radio id: $artistId");
      artistId = r.nextInt(artistsCount) + 1;
    }

    debugPrint("############### artist radio id: $artistId");

    // get random track
    Tracks tracksCtrl = await artistsAPI.fetchRandomTrack(artistId);

    tracksCtrl.results[0].playlistMeta = new PlaylistMeta(
          -1, "", tracksCtrl, TrackPlaylistType.RANDOM);

    return tracksCtrl.results[0];
  }
}
