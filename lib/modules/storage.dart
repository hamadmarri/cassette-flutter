import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  static Future clear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  static Future contains(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(key);
  }

  static Future storeInt(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value);
  }

  static Future<int> retrieveInt(String key) async {
    var prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key);
  }

  static Future storeString(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<String> retrieveString(String key) async {
    var prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
}
