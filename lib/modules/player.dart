import 'dart:async';
import 'dart:io';

import 'package:cassette/BackendAPI/tracksAPI.dart';
import 'package:cassette/config.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:cassette/BackendAPI/rest.dart';
import 'package:cassette/modules/modules.dart';

import 'limit.dart';

class PlayerModel extends Model {
  static PlayerModel of(BuildContext context) =>
      ScopedModel.of<PlayerModel>(context);

  PlayerModel() {
    audioPlayer = new AudioPlayer();

    // when repeat once these will not release track, i.e. cached
    audioPlayer.setReleaseMode(ReleaseMode.STOP);
    audioPlayer.onDurationChanged.listen(_onDuration);
    audioPlayer.onAudioPositionChanged.listen(_onPosition);
    audioPlayer.onPlayerCompletion.listen(_onComplete);

    // initialize the limit
    Limit.init();
    _timerLimits();
  }

  AudioPlayer audioPlayer;
  Track currentTrack;
  bool isPlaying = false;
  Duration duration;
  Duration position;

  void play(Track track) async {
    // AudioPlayer.logEnabled = true;

    // check if timeout then don't play
    if (Limit.timeout) return;

    if (currentTrack != track) {
      // remove track from cache
      audioPlayer.release();
    }

    currentTrack = track;

    if (isPlaying) {
      audioPlayer.stop();
    }

    _play(track);

    duration = null;
    position = null;

    isPlaying = true;
    notifyListeners();
  }

  void _play(Track track) {
    // if local, track id would be negative
    if (track.id < 0) {
      // local from device
      audioPlayer.play(track.link, isLocal: true);
    } else {
      // online
      String url = constructStreamUrl(track);
      audioPlayer.play(url);
      _listen(track);
    }
  }

  String constructStreamUrl(Track track) {
    if (Platform.isAndroid) {
      return Rest.streamUrlSecure +
          track.artists[0].name +
          "/" +
          track.name +
          ".mp3";
    } else {
      return Uri.encodeFull(
          Rest.streamUrl + track.artists[0].name + "/" + track.name + ".mp3");
    }
  }

  void stop() {
    audioPlayer.stop();
    isPlaying = false;
    notifyListeners();
  }

  void pause() {
    audioPlayer.pause();
    isPlaying = false;
    notifyListeners();
  }

  void resume() {
    // check if timeout then don't play
    if (Limit.timeout) return;

    if (currentTrack != null) {
      audioPlayer.resume();
      isPlaying = true;
      notifyListeners();
    }
  }

  void _onDuration(Duration d) {
    if (duration == null) {
      duration = d;
      notifyListeners();
    }
  }

  void _onPosition(Duration p) {
    position = p;
  }

  void _onComplete(void event) {
    notifyListeners();
  }

  void _listen(Track track) {
    TracksAPI tracksAPI = new TracksAPI();
    tracksAPI.listen(track.id);
    track.listenedTimes++;
  }

  void _timerLimits() {
    if (!Config.SHOW_ADS) return;
    
    Timer.periodic(new Duration(seconds: 10), (timer) {
      if (isPlaying) {
        Limit.checkLimit();
      }
    });
  }
}
