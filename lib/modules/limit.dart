import 'package:cassette/modules/storage.dart';
import 'package:flutter/material.dart';

class Limit {
  static String key = "limit";
  static const int MAX_SECONDS = 15 * 60; // 15 minutes
  static int seconds = MAX_SECONDS;
  static bool timeout = false;
  static bool navigated = false;

  static init() {
    Storage.contains(key).then((value) {
      if (value == false) {
        // if first time then create one
        Storage.storeInt(key, seconds);
      } else {
        // read remaining seconds
        Storage.retrieveInt(key).then((v) {
          seconds = v;
          if (seconds <= 0) {
            timeout = true;
          }
        });
      }
    });
  }

  static bool checkLimit() {
    seconds -= 10;
    Storage.storeInt(key, seconds);

    debugPrint(seconds.toString());

    if (seconds <= 0) {
      timeout = true;
      return true;
    }

    return false;
  }

  static refill() {
    seconds = MAX_SECONDS;
    Storage.storeInt(key, seconds).then((v) {
      timeout = false;
    });
  }
}
