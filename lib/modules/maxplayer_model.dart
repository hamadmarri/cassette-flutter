import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class MaxPlayerModel extends Model {
  static MaxPlayerModel of(BuildContext context) =>
      ScopedModel.of<MaxPlayerModel>(context);

  bool faved = false;
  bool downloadComplete = false;
  int downloadProgress = -1;

  reset() {
    faved = false;
    downloadComplete = false;
    downloadProgress = -1;
    notifyListeners();
  }
}
