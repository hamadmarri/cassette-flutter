import 'package:json_annotation/json_annotation.dart';

/// This allows the `Track` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'modules.g.dart';

enum TrackPlaylistType { TRACKS, DOWNLOADED_LIST, DOWNLOADED_DEVICE, RANDOM }

class PlaylistMeta {
  PlaylistMeta(this.id, this.from, this.tracksCtrl, this.playlistType);

  int id;
  String from;
  TrackPlaylistType playlistType;
  Tracks tracksCtrl;
}

@JsonSerializable()
class Track {
  int id;
  String name;
  List<Artist> artists;
  int trackdetails;

  @JsonKey(ignore: true)
  PlaylistMeta playlistMeta;

  @JsonKey(name: 'listened_times')
  int listenedTimes;

  String link;
  int album;

  @JsonKey(name: 'original_song')
  int originalSong;

  Track(this.id, this.name, this.artists, this.trackdetails, this.listenedTimes,
      this.link, this.album, this.originalSong);

  factory Track.fromJson(Map<String, dynamic> json) => _$TrackFromJson(json);
  Map<String, dynamic> toJson() => _$TrackToJson(this);
}

@JsonSerializable()
class Tracks {
  int count;
  String next;
  String previous;
  List<Track> results;

  Tracks(this.count, this.next, this.previous, this.results);

  factory Tracks.fromJson(Map<String, dynamic> json) => _$TracksFromJson(json);
  Map<String, dynamic> toJson() => _$TracksToJson(this);
}

@JsonSerializable()
class Artist {
  int id;
  String name;
  String country;

  @JsonKey(name: 'track_count')
  int trackCount;

  @JsonKey(name: 'subscribe_count')
  int subscribeCount;
  String file;

  Artist(this.id, this.name, this.trackCount, this.subscribeCount, this.file,
      this.country);

  factory Artist.fromJson(Map<String, dynamic> json) => _$ArtistFromJson(json);
  Map<String, dynamic> toJson() => _$ArtistToJson(this);
}

@JsonSerializable()
class Artists {
  int count;
  String next;
  String previous;
  List<Artist> results;

  Artists(this.count, this.next, this.previous, this.results);

  factory Artists.fromJson(Map<String, dynamic> json) =>
      _$ArtistsFromJson(json);
  Map<String, dynamic> toJson() => _$ArtistsToJson(this);
}

@JsonSerializable()
class Hashtag {
  int id;
  String name;
  int playlist;

  @JsonKey(name: 'favorites_count')
  int favoritesCount;

  Hashtag(this.id, this.name, this.playlist, this.favoritesCount);

  factory Hashtag.fromJson(Map<String, dynamic> json) =>
      _$HashtagFromJson(json);
  Map<String, dynamic> toJson() => _$HashtagToJson(this);
}

@JsonSerializable()
class Hashtags {
  int count;
  String next;
  String previous;
  List<Hashtag> results;

  Hashtags(this.count, this.next, this.previous, this.results);

  factory Hashtags.fromJson(Map<String, dynamic> json) =>
      _$HashtagsFromJson(json);
  Map<String, dynamic> toJson() => _$HashtagsToJson(this);
}

@JsonSerializable()
class UserDetails {
  int id;
  Map user = {"first_name": null, "last_name": null};

  UserDetails(this.id, this.user);

  factory UserDetails.fromJson(Map<String, dynamic> json) =>
      _$UserDetailsFromJson(json);
  Map<String, dynamic> toJson() => _$UserDetailsToJson(this);
}
