import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:scoped_model/scoped_model.dart';

class FetchLoadingModel extends Model {
  Function callback;
  ScrollController scrollController;
  bool isLoading = false;
  bool showGoToTop = false;

  initCallback(_callback) {
    callback = _callback;
  }

  initScrollController() {
    // debugPrint("getScrollController");
    scrollController = ScrollController()..addListener(_scrollListner);
  }

  _scrollListner() async {
    if (!isLoading &&
        scrollController.position.pixels ==
            scrollController.position.maxScrollExtent) {
      // debugPrint("called");

      isLoading = true;
      notifyListeners();

      await callback();

      isLoading = false;
      notifyListeners();
    }

    if (scrollController.position.pixels > 100) {
      showGoToTop = true;
    }
  }

  loadingIndicator() {
    return isLoading
        ? SpinKitSpinningCircle(
            color: Colors.blue.withOpacity(0.32),
            size: 50.0,
          )
        : Container();
  }

  FloatingActionButton showGoToTopButton() {
    return showGoToTop
        ? FloatingActionButton(
            backgroundColor: Colors.white.withOpacity(0.08),
            foregroundColor: Colors.white.withOpacity(0.16),
            elevation: 0.0,
            child: Icon(
              Icons.arrow_upward,
            ),
            onPressed: () {
              scrollController.animateTo(
                0,
                duration: new Duration(seconds: 1),
                curve: Curves.fastLinearToSlowEaseIn,
              );

              showGoToTop = false;
              notifyListeners();
            },
          )
        : null;
  }

  startLoading() {
    isLoading = true;
    notifyListeners();
  }

  finishLoading() {
    isLoading = false;
    notifyListeners();
  }

  static FetchLoadingModel of(BuildContext context) =>
      ScopedModel.of<FetchLoadingModel>(context);
}
