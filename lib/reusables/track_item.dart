import 'package:cassette/modules/play_list.dart';
import 'package:cassette/modules/player.dart';
import 'package:cassette/modules/share.dart';
import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:cassette/modules/modules.dart';

class TrackItem extends StatelessWidget {
  const TrackItem({Key key, this.track, this.actions}) : super(key: key);

  final Track track;
  final Function actions;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      delegate: new SlidableDrawerDelegate(),
      actionExtentRatio: 0.2,
      actions: (actions == null) ? _defaultActions(track) : actions(track),
      child: ListTile(
        onTap: () {
          // play track
          PlayerModel.of(context).play(track);

          // rest play list for new list
          PlayListModel.of(context).restPlaylist();
        },
        title: Text(track.name),
        subtitle: Text(track.artists[0].name),
        leading: SizedBox(
          height: 50,
          width: 50,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: track.artists[0].file.isNotEmpty
                ? Image.network(
                    track.artists[0].file,
                    fit: BoxFit.cover,
                  )
                : Image.asset("assets/images/icon_logo.png"),
          ),
        ),
        trailing: _trailing(track.listenedTimes),
        isThreeLine: true,
      ),
    );
  }

  SizedBox _trailing(int listenedTimes) {
    final compact = NumberFormat.compact();

    return SizedBox(
      width: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 4.0),
            child: Text(compact.format(listenedTimes).toString()),
          ),
          Icon(Icons.equalizer),
        ],
      ),
    );
  }

  List<Widget> _defaultActions(Track track) {
    return <Widget>[
      // new IconSlideAction(
      //   caption: 'تحميل',
      //   color: Colors.blue,
      //   icon: Icons.cloud_download,
      //   onTap: () => debugPrint('تحميل ${track.toString()}'),
      // ),
      new IconSlideAction(
        caption: 'مشاركة',
        color: Colors.pink[400],
        icon: Icons.share,
        onTap: () => ShareModule.shareTrack(track),
      ),
    ];
  }
}
