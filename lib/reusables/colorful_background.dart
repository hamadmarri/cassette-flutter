import 'dart:math';

import 'package:flutter/material.dart';
// import 'dart:math';

class ColorfulBackground extends BoxDecoration {
  ColorfulBackground()
      : super(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            stops: [0.764, 1],
            colors: [
              Color(lightColor()),
              Color(touchColor()),
            ],
          ),
        );

  static int touchColor() {
    return 0xff91D7F2;
  }

  static int lightColor() {
    return 0xfff2fbff;
  }
}


// 0xffefc5ef 0xff7ffdfb 0xffefdfef 