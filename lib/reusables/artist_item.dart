import 'package:cassette/navigator.dart';
import 'package:cassette/pages/artist.dart';
import 'package:intl/intl.dart';

import 'package:flutter/material.dart';

import 'package:cassette/modules/modules.dart';

class ArtistItem extends StatelessWidget {
  const ArtistItem({
    Key key,
    @required this.artist,
  }) : super(key: key);

  final double itemHeight = 260.0;
  final double itesWidth = 260.0 * 0.618;
  final double itemImageHeight = 260.0 * 0.618;

  final Artist artist;

  @override
  Widget build(BuildContext context) {
    final NumberFormat compact = NumberFormat.compact();

    // Golden ration: 0.618, 129.78, 80.20404, 49.56609672, 30.631847773
    return Container(
      height: itemHeight,
      margin: EdgeInsets.only(left: 8.0),
      width: itesWidth,
      child: GestureDetector(
        onTap: () {
          goTo(context, ArtistPage(artist: artist));
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(21.0),
          child: Card(
            child: Stack(
              children: <Widget>[
                SizedBox(
                  height: itemImageHeight,
                  width: itesWidth,
                  child: Image.network(
                    artist.file,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  top: itemImageHeight + 8.95143523,
                  right: 0.0,
                  left: 0.0,
                  child: SizedBox(
                    width: itesWidth,
                    child: ListTile(
                        title: Text(
                          artist.name,
                          overflow: TextOverflow.ellipsis,
                        ),
                        subtitle: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.music_note),
                            Text(
                              compact.format(artist.trackCount).toString(),
                            )
                          ],
                        )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
