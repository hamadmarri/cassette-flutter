import 'dart:ui';

// import 'package:admob_flutter/admob_flutter.dart';
// import 'package:cassette/modules/ads.dart';
import 'package:cassette/BackendAPI/authAPI.dart';
import 'package:cassette/modules/modules.dart';
import 'package:cassette/modules/play_list.dart';
import 'package:cassette/modules/player.dart';
import 'package:cassette/navigator.dart';
import 'package:cassette/pages/downloaded_tracks.dart';
import 'package:cassette/pages/landing.dart';
import 'package:cassette/pages/profile.dart';
import 'package:cassette/pages/search.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

enum NavPage { profile, library, home, search, others }

class BottomBar extends StatelessWidget {
  const BottomBar({Key key, this.page}) : super(key: key);

  final NavPage page;

  @override
  Widget build(BuildContext context) {
    var iconSize = 34.0;
    var activeColor = Colors.lightBlue; //Color(0xff0388A6);
    var iconColor = Colors.blueGrey[400];

    return Positioned(
      bottom: 0.0,
      // top: MediaQuery.of(context).size.height - 85,
      left: 0.0,
      right: 0.0,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 80, sigmaY: 80),
          child: Container(
            color: Colors.white.withOpacity(0.70),
            height: 85.0,
            // elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 13.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  ScopedModelDescendant(
                    rebuildOnChange: true,
                    builder: (BuildContext context, Widget child,
                        PlayListModel model) {
                      bool isRadio = (model.mode == PlayOptions.RADIO);
                      return IconButton(
                        onPressed: () {
                          if (isRadio) {
                            model.changeModeFromRadio();
                          } else {
                            model.changeModeToRadio();
                            _playRadio(context);
                          }
                        },
                        color: isRadio ? Colors.orange : iconColor,
                        iconSize: iconSize,
                        highlightColor: Colors.red,
                        padding: EdgeInsets.all(0.0),
                        icon: Icon(Icons.radio),
                      );
                    },
                  ),
                  IconButton(
                    onPressed: () {
                      if (page != NavPage.search) goTo(context, SearchPage());
                    },
                    color: page == NavPage.search ? activeColor : iconColor,
                    iconSize: iconSize,
                    highlightColor: Colors.red,
                    padding: EdgeInsets.all(0.0),
                    icon: Icon(Icons.search),
                  ),
                  IconButton(
                    onPressed: () {
                      if (page != NavPage.home) goTo(context, LandingPage());
                    },
                    color: page == NavPage.home ? activeColor : iconColor,
                    iconSize: iconSize,
                    highlightColor: Colors.red,
                    padding: EdgeInsets.all(0.0),
                    icon: Icon(Icons.home),
                  ),
                  IconButton(
                    onPressed: () {
                      if (page != NavPage.library)
                        goTo(context, DownloadedTracks());
                    },
                    color: page == NavPage.library ? activeColor : iconColor,
                    iconSize: iconSize,
                    highlightColor: Colors.red,
                    padding: EdgeInsets.all(0.0),
                    icon: Icon(Icons.library_music),
                  ),
                  IconButton(
                    onPressed: () {
                      // no profile for guests
                      if (AuthAPI.isGuest) {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text("غير متاح للضيف"),
                          ),
                        );
                        return;
                      }

                      if (page != NavPage.profile) goTo(context, ProfilePage());
                    },
                    color: page == NavPage.profile ? activeColor : iconColor,
                    iconSize: iconSize,
                    highlightColor: Colors.red,
                    padding: EdgeInsets.all(0.0),
                    icon: Icon(Icons.person),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future _playRadio(BuildContext context) async {
    // TracksAPI tracksAPI = new TracksAPI();
    // Tracks randomCtrl = await tracksAPI.fetchRandom10();

    // randomCtrl.results.forEach((t) {
    //   return t.playlistMeta = new PlaylistMeta(
    //       -1, "getRandomTrack", randomCtrl, TrackPlaylistType.RANDOM);
    // });

    Track track = await PlayListModel.of(context).getRadio();
    PlayerModel.of(context).play(track);
  }
}
