import 'dart:ui';

import 'package:flutter/material.dart';

class TransparentAppBar {
  static AppBar _appBar;
  static double height;

  static Widget appBar(title) {
    _appBar = AppBar(
      backgroundColor: Colors.white.withOpacity(0.13),
      title: Text(title),
    );

    height = _appBar.preferredSize.height;
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 80, sigmaY: 80),
        child: _appBar,
      ),
    );
  }
}
