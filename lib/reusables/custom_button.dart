import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key key,
    this.onPressed,
    this.text,
  }) : super(key: key);

  final Function onPressed;
  final text;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.blue.withOpacity(0.32),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(13),
      ),
      onPressed: onPressed,
      child: Text(
        text,
      ),
    );
  }
}
