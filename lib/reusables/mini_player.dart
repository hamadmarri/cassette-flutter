import 'dart:async';
import 'dart:ui';

import 'package:audioplayers/audioplayers.dart';
import 'package:cassette/modules/limit.dart';
import 'package:cassette/modules/modules.dart';
import 'package:cassette/modules/play_list.dart';
import 'package:cassette/modules/player.dart';
import 'package:cassette/modules/share.dart';
import 'package:cassette/navigator.dart';
import 'package:cassette/pages/max_player/max_player.dart';
import 'package:cassette/pages/timeout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:scoped_model/scoped_model.dart';

import '../config.dart';

class MiniPlayer extends StatefulWidget {
  MiniPlayer({Key key}) : super(key: key);

  @override
  _MiniPlayerState createState() => _MiniPlayerState();
}

class _MiniPlayerState extends State<MiniPlayer> {
  final double height = 71.0;

  @override
  void initState() {
    if (Config.SHOW_ADS) {
      Timer.periodic(new Duration(seconds: 1), (timer) {
        if (Limit.timeout && !Limit.navigated) {
          if (PlayerModel.of(context).isPlaying) {
            PlayerModel.of(context).pause();
          }
          goTo(context, TimeoutPage());
        }
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 85.0, // + 50.0, // + 50 for the banner
      child: GestureDetector(
        onTap: () {
          goTo(context, MaxPlayer());
        },
        child: Slidable(
          delegate: new SlidableDrawerDelegate(),
          actionExtentRatio: 0.2,
          actions: _actions(),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 80, sigmaY: 80),
              child: Container(
                height: height,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.13),
                  border: Border(
                    top: BorderSide(
                      color: Color(0xff0388A6),
                      width: 0.13,
                    ),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 21.0),
                  child: Row(
                    children: <Widget>[
                      new Controls(),
                      new TrackInfo(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _actions() {
    return <Widget>[
      new IconSlideAction(
        caption: 'مشاركة',
        color: Colors.pink[400],
        icon: Icons.share,
        onTap: () =>
            ShareModule.shareTrack(PlayerModel.of(context).currentTrack),
      ),
    ];
  }
}

class Controls extends StatelessWidget {
  const Controls({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        IconButton(
          color: Color(0xff02212E).withOpacity(0.62),
          iconSize: 42,
          icon: Icon(
            Icons.skip_next,
          ),
          onPressed: () async {
            Track currentTrack = PlayerModel.of(context).currentTrack;
            Track nextTrack =
                await PlayListModel.of(context).getAutoNextTrack(currentTrack);

            if (nextTrack != null) {
              PlayerModel.of(context).play(nextTrack);
            } else {
              // stop playing
              PlayerModel.of(context).stop();
              PlayerModel.of(context).currentTrack = null;
            }
          },
        ),
        IconButton(
          color: Color(0xff02212E).withOpacity(0.62),
          iconSize: 48,
          icon: ScopedModelDescendant<PlayerModel>(
            builder: (BuildContext context, Widget child, PlayerModel model) {
              if (model.audioPlayer.state == AudioPlayerState.COMPLETED) {
                _autoPlayNext(context);
              }
              if (model.isPlaying)
                return Icon(Icons.pause);
              else
                return Icon(Icons.play_arrow);
            },
          ),
          onPressed: () {
            var player = PlayerModel.of(context);
            if (player.isPlaying)
              player.pause();
            else
              player.resume();
          },
        ),
      ],
    );
  }

  Future _autoPlayNext(context) async {
    Track currentTrack = PlayerModel.of(context).currentTrack;
    Track nextTrack =
        await PlayListModel.of(context).getAutoNextTrack(currentTrack);

    if (nextTrack != null) {
      // if not same track
      if (currentTrack != nextTrack) {
        PlayerModel.of(context).play(nextTrack);
      } else {
        // if same track just seek,
        // no need to fetch from server again
        PlayerModel.of(context).resume();
      }
    } else {
      // stop playing
      PlayerModel.of(context).stop();
      PlayerModel.of(context).currentTrack = null;
    }
  }
}

class TrackInfo extends StatelessWidget {
  const TrackInfo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: const EdgeInsets.all(13.0),
              child: ScopedModelDescendant<PlayerModel>(
                  builder: (context, child, model) {
                return Text(
                  model.currentTrack != null ? model.currentTrack.name : '',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Color(0xff260A14)),
                );
              }),
            ),
          ),
          SizedBox(
              height: 50.0,
              width: 50.0,
              child: ClipRRect(
                borderRadius: new BorderRadius.circular(13.0),
                child: ScopedModelDescendant<PlayerModel>(
                    builder: (context, child, model) {
                  if (model.currentTrack != null &&
                      model.currentTrack.artists[0].file.isNotEmpty) {
                    return Image.network(
                      model.currentTrack.artists[0].file,
                      fit: BoxFit.cover,
                    );
                  } else {
                    return Image.asset("assets/images/icon_logo_ios.png");
                    // return Icon(
                    //   Icons.audiotrack,
                    //   size: 42,
                    //   color: Colors.pink.withOpacity(0.21),
                    // );
                  }
                }),
              )),
        ],
      ),
    );
  }
}
