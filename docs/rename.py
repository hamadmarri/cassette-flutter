#-*- coding: utf-8 -*-
import os 
import re


artist_1 = "اصالة نصري"
artist_2 = "اصاله نصري"
artist_3 = "اصالة"
artist_4 = "اصاله"


for filename in os.listdir("."):
    if filename == "rename.py" or filename == "filenames.txt" or filename == "index.html":
        continue

    # strip the end
    newfilename = filename[:-16] + ".mp3"

    # replace text
    newfilename = newfilename.replace("أ", "ا")
    newfilename = newfilename.replace("إ", "ا")
    newfilename = newfilename.replace("آ", "ا")

    newfilename = newfilename.replace("َ", "")
    newfilename = newfilename.replace("ً", "")
    newfilename = newfilename.replace("ُ", "")
    newfilename = newfilename.replace("ٌ", "")
    newfilename = newfilename.replace("ِ", "")
    newfilename = newfilename.replace("ٍ", "")
    newfilename = newfilename.replace("ّ", "")
    newfilename = newfilename.replace("ْ", "")

    newfilename = newfilename.replace(artist_1, "")
    newfilename = newfilename.replace(artist_2, "")
    newfilename = newfilename.replace(artist_3, "")
    newfilename = newfilename.replace(artist_4, "")

    
    newfilename = newfilename.replace("_", "")
    newfilename = newfilename.replace("—", "")
    newfilename = newfilename.replace("ـ", "")
    newfilename = newfilename.replace("-", "")
    newfilename = newfilename.replace("'", "")
    newfilename = newfilename.replace("#", "")
    newfilename = newfilename.replace("&", " و ")
    newfilename = newfilename.replace(",", " ")
    newfilename = newfilename.replace("يوتيوب", "")
    newfilename = newfilename.replace("كلمات", "")
    newfilename = newfilename.replace("الفيديو", "")
    newfilename = newfilename.replace("الفديو", "")
    newfilename = newfilename.replace("فديو", "")
    newfilename = newfilename.replace("فيديو", "")
    newfilename = newfilename.replace("كليب", "")
    newfilename = newfilename.replace("كليب", "")
    newfilename = newfilename.replace("النسخة الاصلية", "")
    newfilename = newfilename.replace("النسخة الاصليه", "")
    newfilename = newfilename.replace("النسخه الاصلية", "")
    newfilename = newfilename.replace("النسخه الاصليه", "")
    newfilename = newfilename.replace("افضل تسجيل", "")
    newfilename = newfilename.replace(" نسخة ", " ")
    newfilename = newfilename.replace(" نسخه ", " ")
    newfilename = newfilename.replace(" نادر ", " ")
    newfilename = newfilename.replace(" الرسمي ", " ")
    newfilename = newfilename.replace(" الرسمى ", " ")



    # regex
    newfilename = re.sub(r'[a-zA-Z]', "", newfilename)

    
    newfilename = newfilename.replace(".3", ".mp3")
    

    # reduce spaces
    temp = newfilename.replace("  ", " ")
    while (temp != newfilename):
        newfilename = temp
        temp = newfilename.replace("  ", " ")

    newfilename = temp


    newfilename = newfilename.replace("( )", " ")
    newfilename = newfilename.replace("()", " ")
    newfilename = newfilename.replace("[ ]", " ")
    newfilename = newfilename.replace("[]", " ")


    # reduce spaces again
    temp = newfilename.replace("  ", " ")
    while (temp != newfilename):
        newfilename = temp
        temp = newfilename.replace("  ", " ")

    newfilename = temp



    newfilename = newfilename.replace(" .mp3", ".mp3")


    if newfilename[0:1] == " ":
        newfilename = newfilename[1:]


    print(filename + "=>" +newfilename)
    os.rename(filename, newfilename)